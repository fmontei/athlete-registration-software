package datastore;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import domain.Athlete;
import domain.Event;
import domain.Heat;
import domain.School;
import domain.Teacher;

public class AllData implements Serializable{

	private static final long serialVersionUID = 6430636843350794296L;
	
	private ArrayList<Athlete> athletes;
	private ArrayList<Event> events;
	private ArrayList<Heat> heats;
	private ArrayList<School> schools;
	private ArrayList<Teacher> teachers;
	
	private Map<String, Athlete> athleteMap;
	private Map<String, School> schoolMap;
	private Map<String, Teacher> teacherMap;
	private Map<String, Event> eventMap;
	private Map<String, Heat> heatMap;
	
	private static final File athleteFile = new File("athletes.csv");
	private static final File eventFile = new File("events.csv");
	private static final File heatFile = new File("heats.csv");
	
	public AllData() {
		athletes = new ArrayList<Athlete>();
		events = new ArrayList<Event>();
		heats = new ArrayList<Heat>();
		schools = new ArrayList<School>();
		teachers = new ArrayList<Teacher>();
		
		athleteMap = new HashMap<String, Athlete>();
		schoolMap = new HashMap<String, School>();
		teacherMap = new HashMap<String, Teacher>();
		eventMap = new HashMap<String, Event>();
		heatMap = new HashMap<String, Heat>();
		
//		eventAthletesMap = new HashMap<String, ArrayList<Athlete>>();
//		eventAthletes = new ArrayList<Athlete>();
	}
	
	// add athlete to collection
	public void addAthlete(Athlete athlete) throws Exception {
		if(athlete != null && !athletes.contains(athlete)) {
			athletes.add(athlete);
		}
		else {
			throw new Exception("Error: Can't add invalid athlete");
		}
	}
	
	// add event to collection
	public void addEvent(Event event) throws Exception {
		if(event != null && !events.contains(event)) {
			events.add(event);
		}
		else {
			throw new Exception("Error: Can't add invalid event");
		}
	}
	
	// add heat to collection
	public void addHeat(Heat heat) throws Exception {
		if(heat != null && !heats.contains(heat)) {
			heats.add(heat);
		} 
		else {
			throw new Exception("Error: Can't add invalid heat");
		}
	}
	
	// add school to collection
	public void addSchool(School school) throws Exception {
		if(school != null && !schools.contains(school)) {
			schools.add(school);
		} 
		else {
			throw new Exception("Error: Can't add invalid school");
		}
	}
	
	// remove teacher by instance
	public void addTeacher(Teacher teacher) throws Exception {
		if(teacher != null && !teachers.contains(teacher)) {
			teachers.add(teacher);
		}
		else {
			throw new Exception("Error: Can't add invalid teacher");
		}
	}
	
	// remove athlete by instance
	public void removeAthlete(Athlete athlete) throws Exception {
		if(!athletes.remove(athlete)) throw new Exception("Can't remove - Athlete not found");
	}
	
	// remove event by instance
	public void removeEvent(Event event) throws Exception {
		if(!events.remove(event)) throw new Exception("Can't remove - Event not found");
	}
	
	// remove heat by instance
	public void removeHeat(Heat heat) throws Exception {
		if(!heats.remove(heat)) throw new Exception("Can't remove - Heat not found");
	}
	
	// remove school by instance
	public void removeSchool(School school) throws Exception {
		if(!schools.remove(school)) throw new Exception("Can't remove - School not found");
	}
	
	// remove teacher by instance
	public void removeTeacher(Teacher teacher) throws Exception {
		if(!teachers.remove(teacher)) throw new Exception("Can't remove - Teacher not found");
	}
	
	// remove teacher by index
	public void removeAthlete(int index) {
		athletes.remove(index);
	}
	
	// remove event by index
	public void removeEvent(int index) {
		events.remove(index);
	}
	
	// remove heat by index
	public void removeHeat(int index) {
		heats.remove(index);
	}
	
	// remove school by index
	public void removeSchool(int index) {
		schools.remove(index);
	}
	
	// remove teacher by index
	public void removeTeacher(int index) {
		teachers.remove(index);
	}
	
	// pass in: athlete school name + first name + last name
	public Athlete getAthlete(String schoolAndFullName) {
		return athleteMap.get(schoolAndFullName);
	}
	
	// pass in: event name
	public Event getEvent(String eventName) {
		return eventMap.get(eventName);
	}
	
	// pass in: event name + gender + minAge
	public Heat getHeat(String nameGenderAge) {
		return heatMap.get(nameGenderAge);
	}
	
	// pass in: school name
	public School getSchool(String schoolName) {
		return schoolMap.get(schoolName);
	}
	
	// pass in: school name + teacher name
	public Teacher getTeacher(String schoolAndName) {
		return teacherMap.get(schoolAndName);
	}
	
	// get athlete list
	public ArrayList<Athlete> getAthletes() {
		return athletes;
	}

	// get event list
	public ArrayList<Event> getEvents() {
		return events;
	}

	// get heat list
	public ArrayList<Heat> getHeats() {
		return heats;
	}

	// get school list
	public ArrayList<School> getSchools() {
		return schools;
	}

	// get teacher list
	public ArrayList<Teacher> getTeachers() {
		return teachers;
	}
	
	// parse all data 
	public void parseData() {
		try {
			parseEvents();
			parseAthletes();
			parseHeats();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	// parse athletes from athletes.csv into athletes collection
	private void parseAthletes() throws IOException {
		athletes.clear();
		String[] ar; // holds attributes from each line
		Athlete athlete = new Athlete();
		School school = new School();
		Teacher teacher = new Teacher();
		Event event = new Event();

		FileReader fr = new FileReader(athleteFile);
		BufferedReader dataReader = new BufferedReader(fr);
		String line = dataReader.readLine();
		
		// parse athlete attributes
		
		while(dataReader.ready()) {
			// process line of attributes
			line = dataReader.readLine();
			ar = line.split(",");
			for(int i=0; i<ar.length; i++) {
				ar[i] = ar[i].replace("\"","");
			}

			// add new school
			if(!schoolMap.containsKey(ar[1])) {
				school = new School(ar[1]);
				schools.add(school);

				schoolMap.put(school.getName(), school);
			}

			// add new teacher
			if(!teacherMap.containsKey(ar[1] + ar[2])) {
				teacher = new Teacher(ar[2]);
				teacher.setSchool(school);
				school.addTeacher(teacher);
				teachers.add(teacher);

				teacherMap.put(ar[1] + ar[2], teacher);
			}

			// add new athlete
			if(!athleteMap.containsKey(ar[1] + ar[3] + ar[4])) {
				
				athlete = new Athlete(ar[3], ar[4], ar[6], Integer.parseInt(ar[5]), ar[1], Float.parseFloat(ar[9])/100,
						ar[7], ar[8], ar[0], ar[2]);
				
				event = eventMap.get(ar[8]); // get event
				//event.addAthlete(athlete);   // add athlete to Event athletes list
				athlete.setEvent1(event);    // set athlete event1
				teacher.addAthlete(athlete); // add athlete to Teacher athletes list
				athletes.add(athlete);       // add athlete to DataStore athlete list

				athleteMap.put(athlete.getSchool() + athlete.getFirstName() + athlete.getLastName(), athlete);
			}
			// register athlete for 2nd event
			else {
				athlete = new Athlete(ar[3], ar[4], ar[6], Integer.parseInt(ar[5]), ar[1], Float.parseFloat(ar[9])/100,
						ar[7], ar[8], ar[0], ar[2]);
				event = eventMap.get(ar[8]);
				//event.addAthlete(athlete);
				athlete.setEvent2(event);
				athletes.add(athlete);
			}

		}

		dataReader.close();
	}

	// parse events from events.csv into events collection
	private void parseEvents() throws IOException {
		String[] ar; // holds attributes from each line

		FileReader fr = new FileReader(eventFile);
		BufferedReader dataReader = new BufferedReader(fr);
		String line = dataReader.readLine();
		events.clear();
		// parse athlete attributes
		while(dataReader.ready()) {
			// process line of attributes
			line = dataReader.readLine();
			ar = line.split(",");
			for(int i=0; i<ar.length; i++) {
				ar[i] = ar[i].replace("\"","");
			}

			// store event in event collection
			Event event = new Event(ar[0], ar[1], ar[2], Integer.parseInt(ar[3]),
					Integer.parseInt(ar[4]), Integer.parseInt(ar[5]));
			events.add(event);

			eventMap.put(event.getName(), event);
		}
		dataReader.close();

	}

	// parse heats from heats.csv into heats collection
	private void parseHeats() throws IOException {
		String[] ar; // holds attributes from each line

		FileReader fr = new FileReader(heatFile);
		BufferedReader dataReader = new BufferedReader(fr);
		String line = dataReader.readLine();
		heats.clear();
		// parse athlete attributes
		while(dataReader.ready()) {
			// process line of attributes
			line = dataReader.readLine();
			ar = line.split(",");
			for(int i=0; i<ar.length; i++) {
				ar[i] = ar[i].replace("\"","");
			}

			// store heat in heat collection
			Heat heat = new Heat(ar[0], ar[1], ar[2], Integer.parseInt(ar[3]), Integer.parseInt(ar[4]),
					ar[5], Integer.parseInt(ar[6]));
			heat.setEvent(eventMap.get(ar[1]));
			heats.add(heat);
			//Event ev = Event.lookUpByCode(heat.getEventCode());
			//ev.addHeat(heat);
			heatMap.put(heat.getEvent().getName() + heat.getGender() + heat.getMinAge(), heat);
		}
		dataReader.close();

	}

	public void dumpData() {
		System.out.println(athletes.size() + " " + events.size() + " " + heats.size());
//		for(School s : schools) {
//			System.out.println(s);
//		}
//		System.out.println();
//		for(Teacher t : teachers) {
//			System.out.println(t);
//			for(Athlete a : t.getAthletes()) {
//				System.out.println("\t" + a);
//				System.out.println("\t\t" + a.getEvent1());
//				if(a.hasTwoEvents()) System.out.println("\t\t" + a.getEvent2());
//			}
//		}
//		System.out.println();
		for(Event e : events) {
			System.out.println(e);
			ArrayList<Athlete> eventAthletes = e.getAthletes();
			for(Athlete a : eventAthletes) {
				System.out.println("\t\t" + a);
			}
		}
		
//		for(Heat h : heats) {
//			System.out.println(h);
//			ArrayList<Athlete> heatAthletes = h.getAthletes();
//			for(Athlete a : heatAthletes) {
//				System.out.println(a);
//			}
//		}
	}
		
}
