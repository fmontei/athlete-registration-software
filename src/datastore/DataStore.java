package datastore;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Vector;

import domain.Athlete;
import domain.Event;
import domain.Heat;
import domain.School;
import domain.Teacher;

public class DataStore {

	private static DataStore instance = null;
	private static AllData allData = new AllData();
	private static int count = 0;
	private static final File allDataFile = new File("data.dat");
	
	private DataStore() {
	}
	
	public void initialize() {
		System.out.println("Loading data...");
		try {
			loadAllData();

		} catch(Exception e) {
			try{
				allData.parseData();
				
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	
	public static DataStore getInstance() {
		count++;
		if(instance == null){
			instance = new DataStore();
		}
		return instance;
	}
	
	// add athlete to collection
	public void addAthlete(Athlete athlete) throws Exception {
		allData.addAthlete(athlete);
	}
	
	// add event to collection
	public void addEvent(Event event) throws Exception {
		allData.addEvent(event);
	}
	
	// add heat to collection
	public void addHeat(Heat heat) throws Exception {
		allData.addHeat(heat);
	}
	
	// add school to collection
	public void addSchool(School school) throws Exception {
		allData.addSchool(school);
	}
	
	// remove teacher by instance
	public void addTeacher(Teacher teacher) throws Exception {
		allData.addTeacher(teacher);
	}
	
	// remove athlete by instance
	public void removeAthlete(Athlete athlete) throws Exception {
		allData.removeAthlete(athlete);
	}
	
	// remove event by instance
	public void removeEvent(Event event) throws Exception {
		allData.removeEvent(event);
	}
	
	// remove heat by instance
	public void removeHeat(Heat heat) throws Exception {
		allData.removeHeat(heat);
	}
	
	// remove school by instance
	public void removeSchool(School school) throws Exception {
		allData.removeSchool(school);
	}
	
	// remove teacher by instance
	public void removeTeacher(Teacher teacher) throws Exception {
		allData.removeTeacher(teacher);
	}	
	
	// remove athlete by index
	public void removeAthlete(int index) {
		allData.removeAthlete(index);
	}
	
	// remove event by index
	public void removeEvent(int index) {
		allData.removeAthlete(index);
	}
	
	// remove heat by index
	public void removeHeat(int index) {
		allData.removeHeat(index);
	}
	
	// remove school by index
	public void removeSchool(int index) {
		allData.removeSchool(index);
	}
	
	// remove teacher by index
	public void removeTeacher(int index) {
		allData.removeTeacher(index);
	}
	
	// pass in: athlete school name + first name + last name
	public Athlete getAthlete(String schoolAndFullName) {
		return allData.getAthlete(schoolAndFullName);
	}

	// pass in: event name
	public Event getEvent(String eventName) {
		return allData.getEvent(eventName);
	}

	// pass in: event name + gender + minAge
	public Heat getHeat(String nameGenderAge) {
		return allData.getHeat(nameGenderAge);
	}

	// pass in: school name
	public School getSchool(String schoolName) {
		return allData.getSchool(schoolName);
	}

	// pass in: school name + teacher name
	public Teacher getTeacher(String schoolAndName) {
		return allData.getTeacher(schoolAndName);
	}

	// get athlete list
	public ArrayList<Athlete> getAthletes() {
		return allData.getAthletes();
	}

	// get event list
	public ArrayList<Event> getEvents() {
		return allData.getEvents();
	}

	// get heat list
	public ArrayList<Heat> getHeats() {
		return allData.getHeats();
	}

	// get school list
	public ArrayList<School> getSchools() {
		return allData.getSchools();
	}

	// get teacher list
	public ArrayList<Teacher> getTeachers() {
		return allData.getTeachers();
	}
	
	private void loadAllData() throws IOException, ClassNotFoundException {
		// create data file if does not exist
		if(!allDataFile.exists()) {
			allDataFile.createNewFile();
			throw new ClassNotFoundException();
		}
		
		FileInputStream fis = new FileInputStream(allDataFile);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		allData = (AllData)ois.readObject();
		ois.close();
	}
	
	public void saveAllData() throws IOException {
		FileOutputStream fos = new FileOutputStream(allDataFile);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(allData);
		oos.close();
	}
	
	public void dumpData() {
		allData.dumpData();
	}
	
	public void loadComboBoxData() {
		DataStore data = DataStore.getInstance();
		ArrayList <Teacher> allTeachers = data.getTeachers();
		ArrayList <School>  getSchoolList  = data.getSchools();
	
		/* Load schools */
		for (School s : getSchoolList) {
			School.getSchoolList().put(s.getName(), new Vector <Teacher> ());
		}
		
		/* Add teachers to school they belong to */
		for (Teacher t : allTeachers) {
			final String schoolName = t.getSchool().getName();
			if (School.getSchoolList().containsKey(schoolName)) {
				School.getSchoolList().get(schoolName).add(t);
			}
		}
		
		/* Load group codes */
		for (Athlete curAthlete : data.getAthletes()) {
			if (!Teacher.getGroupCodes().contains(curAthlete.getGrpCode()))
				Teacher.getGroupCodes().add(curAthlete.getGrpCode());
		}
		
		data = null;
	}
}
