package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AthleteCreateController;
import controller.AthleteMaintainController;
import datastore.AllData;
import domain.Athlete;
import domain.School;
import domain.Teacher;

/* Maintenance panels are panels located on the right-hand side of the program.
 * They are responsible for adding and editing objects to the table to which
 * they correspond (correspondence implied by each class's name) */
public class AthleteCreatePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private AthleteTableModel model;
	private JTable table;
	private NavMenu navMenu;
	
	private JPanel top, bottom;
	private JButton saveButton;
	private JTextField firstField, lastField, ageField, eventNameField;
	private EventComboBox eventCodeBox;
	private JComboBox genderBox, grpBox, schoolBox, teacherBox;
	private ScorePanel scoreField;
	
	private AthleteCreateController createController = 
			new AthleteCreateController();
	
	public AthleteCreatePanel(
			final AthleteTableModel model, 
			final JTable table) {
		this.table = table;
		this.model = model;
		
		this.setLayout(new BorderLayout());
		JPanel buttons = new JPanel(new GridLayout(0, 2));
		
		top = new JPanel(new BorderLayout());
		bottom = new JPanel(new BorderLayout());
		
		/* Function adds appropriate action listeners to add/remove buttons */
		this.configureButtons();
		buttons.add(saveButton);
		
		/* JLabel and JTextField for each appropriate field relating to Athlete 
		 * class */
		final JLabel nameLabel1 = new JLabel("First Name");
		final JLabel nameLabel2 = new JLabel("Last Name");
		final JLabel ageLabel = new JLabel("Age");
		final JLabel genderLabel = new JLabel("Gender");
		final JLabel scoreLabel = new JLabel("Qualifying Score");
		final JLabel schoolLabel = new JLabel("School Name");
		final JLabel teacherLabel = new JLabel("Teacher Name");
		final JLabel eventCodeLabel = new JLabel("Event Code");
		final JLabel eventNameLabel = new JLabel("Event Name");
		final JLabel grpCodeLabel = new JLabel("Group Code");
		firstField = new JTextField("");
		lastField = new JTextField("");
		ageField = new JTextField("");
		scoreField = new ScorePanel();
		eventNameField = new JTextField("");	
		eventNameField.setEditable(false);
		
		/* Declares ComboBoxes for listing "restricted" data that is disallowed 
		 * from being easily and arbitrarily changed by the user */
		eventCodeBox = EventComboBox.getInstance();
		genderBox = new JComboBox (Athlete.GENDERS);
		genderBox.setSelectedIndex(-1);
		grpBox = new JComboBox ();
		grpBox.setSelectedIndex(-1);
		schoolBox = new JComboBox();
		teacherBox = new JComboBox();
		
		/* Associate event name with event code via action listener */
		eventCodeBox.addRelationshipListener(eventNameField);
		/* Listener that triggers unit label changes */
		eventCodeBox.addUnitListener(scoreField);
		
		this.populateSchoolBox(School.getListOfSchoolNames());
		this.populateGroupBox(Teacher.getGroupCodes());
		
		/* Associte school name with teacher names that are populated inside 
		 * teacherBox */
		schoolBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createController.populateTeacherBox(schoolBox, teacherBox);
			}
		});
		
		final JPanel modiPanel = new JPanel(new GridBagLayout());
		final GroupLayout groupLayout = new GroupLayout(modiPanel);
		modiPanel.setLayout(groupLayout);
		groupLayout.setAutoCreateGaps(true);
		groupLayout.setAutoCreateContainerGaps(true);	
		
		/* Group Layout organizes labels and fields into cohesive
		 * and logical groups, separated by uniform gaps */
		groupLayout.setHorizontalGroup(
			groupLayout.createSequentialGroup()
		   		.addGroup(groupLayout.createParallelGroup
		   				(GroupLayout.Alignment.LEADING)
	   				.addComponent(nameLabel1)
	   				.addComponent(nameLabel2)
	   				.addComponent(genderLabel)
	   				.addComponent(ageLabel)
	   				.addComponent(schoolLabel)
	   				.addComponent(teacherLabel)
	   				.addComponent(eventCodeLabel)
	   				.addComponent(eventNameLabel)
	   				.addComponent(scoreLabel)
	   				.addComponent(grpCodeLabel))
	   			.addGroup(groupLayout.createParallelGroup
	   					(GroupLayout.Alignment.LEADING)
   					.addComponent(firstField)
	   				.addComponent(lastField)
	   				.addComponent(ageField)
	   				.addComponent(genderBox)
	   				.addComponent(schoolBox)
	   				.addComponent(teacherBox)
	   				.addComponent(eventCodeBox)
	   				.addComponent(eventNameField)
	   				.addComponent(scoreField)
	   				.addComponent(grpBox))
		);

		/* The vertical and horizontal groups must both be perfectly 
		 * consistent with each other; else a runtime error will be thrown */
		groupLayout.setVerticalGroup(
		   groupLayout.createSequentialGroup()
		   	   .addGroup(groupLayout.createParallelGroup
		   			   (GroupLayout.Alignment.BASELINE)
		           .addComponent(nameLabel1)
		           .addComponent(firstField))
		       .addGroup(groupLayout.createParallelGroup
		    		   (GroupLayout.Alignment.BASELINE)
		           .addComponent(nameLabel2)
		           .addComponent(lastField))
	           .addGroup(groupLayout.createParallelGroup
	        		   (GroupLayout.Alignment.BASELINE)
		           .addComponent(genderLabel)
		           .addComponent(genderBox))
	           .addGroup(groupLayout.createParallelGroup
	        		   (GroupLayout.Alignment.BASELINE)
        		   .addComponent(ageLabel)
		           .addComponent(ageField))
		       .addGroup(groupLayout.createParallelGroup
		    		   (GroupLayout.Alignment.BASELINE)
		           .addComponent(schoolLabel)
		           .addComponent(schoolBox))
	           .addGroup(groupLayout.createParallelGroup
		    		   (GroupLayout.Alignment.BASELINE)
		           .addComponent(teacherLabel)
		           .addComponent(teacherBox))
	           .addGroup(groupLayout.createParallelGroup
	        		   (GroupLayout.Alignment.BASELINE)
		           .addComponent(eventCodeLabel)
		           .addComponent(eventCodeBox))
               .addGroup(groupLayout.createParallelGroup
            		   (GroupLayout.Alignment.BASELINE)
		           .addComponent(eventNameLabel)
		           .addComponent(eventNameField))
	           .addGroup(groupLayout.createParallelGroup
        		   (GroupLayout.Alignment.CENTER)
		           .addComponent(scoreLabel)
		           .addComponent(scoreField))
               .addGroup(groupLayout.createParallelGroup
            		   (GroupLayout.Alignment.BASELINE)
		           .addComponent(grpCodeLabel)
		           .addComponent(grpBox))
		);
		
		top.add(modiPanel, BorderLayout.CENTER);
		top.add(buttons, BorderLayout.PAGE_END);
		top.setBorder(BorderFactory.createTitledBorder("ADD NEW ATHLETE"));
		
		/* Sets up a comment box in case users wish to provide supplementary
		 * information as reminders for themselves or tips/instructions
		 * for other users */
		JTextArea commentBox = new JTextArea("");
		commentBox.setEditable(true);
		commentBox.setBorder(
				BorderFactory.createLineBorder(new Color(99, 150, 225), 1));
		commentBox.setFont(new Font("Serif", Font.PLAIN, 14));
		commentBox.setLineWrap(true);
		commentBox.setWrapStyleWord(true);
		JScrollPane sp = new JScrollPane(commentBox);
		sp.setPreferredSize(new Dimension(100, 100));
		sp.setBorder(BorderFactory.createTitledBorder("COMMENTS"));
		
		bottom.add(sp, BorderLayout.CENTER);
		this.add(top, BorderLayout.CENTER);
		this.add(bottom, BorderLayout.PAGE_END);
	}
	
	/* Configure add button */ 
	public void configureButtons() {
		saveButton = new JButton("Add");
		saveButton.setBackground(new Color(0, 100, 0));
		saveButton.setForeground(Color.WHITE);
		saveButton.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent ev) {
				saveNewAthlete();
			}
		});
	}
	
	public void saveNewAthlete() {
		String errMsg = "Error: Invalid Data Entered";
		ErrDialog errDialog = new ErrDialog(errMsg);

		try {
			createController.createNewAthlete(this);
		} catch(Exception ex) {
			ex.printStackTrace();
			String msg = ex.getLocalizedMessage();
			if (msg == null) return;
			
			if (msg.contains("\"")) {
				msg = msg.substring(msg.indexOf("\""));
				msg = msg.replaceAll("\"", "");
			}
			
			if (msg.equals(ErrDialog.NAME_DNE_ERROR)) {
				errMsg = "Error: athlete's name is incomplete." +
						" Include a first and last name.";
			}
			else if (msg.equals(ErrDialog.GENDER_NS_ERROR)) {
				errMsg = "Error: no gender was selected.";
			}	
			else if (msg.equals(ErrDialog.AGE_WF_ERROR) || 
					msg.equals(ErrDialog.AGE_RANGE_ERROR)) {
				errMsg = "Error: age must be a number between 6 - 99.";
			}
			else if (msg.equals(ErrDialog.EVENTCD_NULL_ERROR)) {
				errMsg = "Error: no events have been created. Create an event" +
						" before attempting to register an athlete.";
				new WarningTimer(navMenu.events);
			}	
			else if (msg.equals(ErrDialog.EVENTCD_NS_ERROR)) {
				errMsg = "Error: no event code was selected.";
			}
			else if (msg.equals(ErrDialog.SCHOOL_NS_ERROR)) {
				errMsg = ErrDialog.SCHOOL_NS_ERROR;
			}
			else if (msg.equals(ErrDialog.TEACHER_NS_ERROR)) {
				errMsg = ErrDialog.TEACHER_NS_ERROR;
			}
			else if (msg.equals(ErrDialog.GRPCD_NS_ERROR)) {
				errMsg = "Error: no group code was selected.";
			}
			else if (msg.contains(ErrDialog.SCORE_RANGE_ERROR)) {
				errMsg = msg;
			}
			else if (msg.equals(ErrDialog.LDR_DNE_ERROR)) {
				errMsg = "Error: leader field is blank.";
			}
			else if (msg.equals(ErrDialog.ATH_REG_ERROR1)) {
				errMsg = "Error: Athlete cannot be registered for same event"
						+ " more than once.";
			}
			else if (msg.equals(ErrDialog.ATH_REG_ERROR2)) {
				errMsg = "Error: Athlete has been registered too many times."
						+ " An athlete can only be registered for 2 events.";
			}
			
			errDialog.setDialog(errMsg);
			errDialog.setVisible(true);
		}
	}
	
	public void populateGroupBox(Vector <String> groups) {
		grpBox.setModel(new DefaultComboBoxModel(groups));
		grpBox.setSelectedIndex(-1);
	}
	public void populateSchoolBox(ArrayList <String> schools) {
		schoolBox.setModel(new DefaultComboBoxModel(schools.toArray()));
		schoolBox.setSelectedIndex(-1);
	}
	public void setNavMenu(NavMenu navMenu) {
		this.navMenu = navMenu;
	}
	
	public AthleteTableModel getModel() {
		return model;
	}
	public JTable getTable() {
		return table;
	}
	public JTextField getFirstNameField() {
		return firstField;
	}
	public JTextField getLastNameField() {
		return lastField;
	}
	public JComboBox getSchoolBox() {
		return schoolBox;
	}
	public JTextField getAgeField() {
		return ageField;
	}
	public ScorePanel getScoreField() {
		return scoreField;
	}
	public JTextField getEventNameField() {
		return eventNameField;
	}
	public JComboBox getCodeBox() {
		return eventCodeBox;
	}
	public JComboBox getGenderDropDown() {
		return genderBox;
	}
	public JComboBox getGrpDropDown() {
		return grpBox;
	}
	public JComboBox getTeacherDropDown() {
		return teacherBox;
	}
	public JComboBox getSchoolDropDown() {
		return schoolBox;
	}
}
