package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class ErrDialog extends JDialog {

	private static final long serialVersionUID = -3165005421837101828L;
	private JTextArea dialog;
	private JPanel flow1;
	private JPanel flow2;
	private JButton okButton;
	
	public final static String AGE_WF_ERROR = 
			"Age must be a number";
	public final static String AGE_RANGE_ERROR = 
			"Athlete must be within range 6 - 99";
	public final static String ATH_REG_ERROR1 = 
			"Athlete registered for same event";
	public final static String ATH_REG_ERROR2 = 
			"Athlete registered too many times";
	public final static String EVENTCD_WF_ERROR = 
			"Event code wrong format";
	public final static String EVENTCD_NS_ERROR = 
			"No event code selected";
	public final static String EVENTCD_NULL_ERROR = 
			"Zero event codes exist";
	public final static String EVENTNM_NS_ERROR = 
			"No event name selected";
	public final static String GENDER_NS_ERROR = 
			"No gender selected";
	public final static String GRPCD_NS_ERROR = 
			"No group code selected";
	public final static String LDR_DNE_ERROR = 
			"Leader field empty.";
	public final static String NAME_DNE_ERROR = 
			"Missing first or last name";
	public final static String SCHOOL_NS_ERROR = 
			"Error: school drop-down must not be left blank.";
	public final static String SCORE_RANGE_ERROR = 
			"Error score is out of range.";
	public final static String SCORE_WF_ERROR = 
			"Score wrong format.";
	public final static String SORT_WF_ERROR = 
			"Sort sequence wrong format.";
	public final static String SORT_DNE_ERROR = 
			"Sort sequence not input.";
	public final static String TEACHER_NS_ERROR = 
			"Error: teacher drop-down must not be left blank.";
	
	public ErrDialog(String dia){
		this(dia, "Parsing Error");
	}
	
	public ErrDialog(String dia, String title) {
		this.setLayout(new GridLayout(2,1));
		this.setTitle(title);
		this.setSize(new Dimension(400, 170));
		this.setModal(true);
		
		flow1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		flow2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		okButton = new JButton("OK");
		okButton.setSize(75, 30);
		okButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);		
			}	
		});
		
		dialog = new JTextArea(dia);
		dialog.setSize(new Dimension(300, 100));
		dialog.setEditable(false);
		dialog.setLineWrap(true);
		dialog.setWrapStyleWord(true);

		flow1.add(dialog);
		flow2.add(okButton);
		add(flow1);
		add(flow2);
		
		int x = Toolkit.getDefaultToolkit().getScreenSize().width;
		x = x / 2;
		x = x - 200;
		int y = Toolkit.getDefaultToolkit().getScreenSize().height;
		y = y / 2;
		y = y - 75;
		this.setLocation(x, y);
	}
	
	public void setDialog(String dia) {
		dialog.setText(dia);
	}
}
