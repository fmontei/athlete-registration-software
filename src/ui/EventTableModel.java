package ui;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import datastore.DataStore;
import domain.Event;

public class EventTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;

	private Vector <Event> vec = new Vector <Event>();
	private String[] columnNames;
	private static DataStore dat;
	public EventTableModel(final String[] columnNames) {
		this.columnNames = columnNames;
	}
	public void popData(){
		dat = DataStore.getInstance();
		vec = new Vector<Event>(dat.getEvents());
	}
	public String getColumnName(int col) {
        return columnNames[col];
    }
	
	public Class getColumnClass(int c)
    {
        return getValueAt(0, c).getClass();
    }

	public int getRowCount() {
		return vec.size();
	}

	public int getColumnCount() {
		return Event.FIELDS;
	}

	public String getValueAt(int row, int col) {
        return vec.get(row).getColumn(col);
    }
	
	public void setValueAt(String val, int rowIndex, int columnIndex) {
		switch(columnIndex) {
		
			case 0:
				vec.get(rowIndex).setCode(val);
				break;
			case 1:
				vec.get(rowIndex).setName(val);
				break;
			case 2:
				vec.get(rowIndex).setScoreUnit(val);
				break;
			case 3:
				vec.get(rowIndex).setScoreMin(Float.parseFloat(val));
				break;
			case 4:
				vec.get(rowIndex).setScoreMax(Float.parseFloat(val));
				break;
			case 5:
				vec.get(rowIndex).setSortSeq(val);
				break;
		}
	}
	
	public void addEvent(Event newEvent) throws Exception {
		/* Prevents the same event code from being added to the table */
		for (Event row : vec) {
			if (row.getCode().equals(newEvent.getCode())) return;
		}
		
		vec.add(newEvent);
		dat.addEvent(newEvent);
	}

	public void deleteEvent(int row) throws Exception {
		dat.removeEvent(vec.get(row));
		vec.remove(row);
	}
	
	public boolean isCellEditable(int row, int col) { 
		
		return vec.get(row).isEditable();
    }
	
	public void setCellEditable(boolean change, int row) {
		
		vec.get(row).setEditable(change);
	}
	
	public Event getEvent(int row ) {
		
		return vec.get(row);
	}
}
