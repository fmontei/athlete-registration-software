package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import controller.AthleteMaintainController;
import controller.EventMaintainController;
import domain.Event;

public class EventMaintainPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private static final String[] columnNames = {Event.EVENT_CODE, 
		Event.EVENT_NAME, Event.SCORE_UNIT, Event.SCORE_MIN, Event.SCORE_MAX, 
		Event.SORT_SEQ};
	
	private static EventTableModel model = new EventTableModel(columnNames);
	private static JTable table = new JTable(model);
	private JPanel gridPanel, lowerPanel;
	private static JTextField eventNameField;
	private EventMaintainController controller = new EventMaintainController();
	private static JTextField sortSeqField;
	private static ScorePanel scoreMinField;
	private static ScorePanel scoreMaxField;
	private static EventComboBox eventCodeBox;
	private static JComboBox scoreBox;
	private JButton save, delete;
	
	public EventMaintainPanel() {
		model.popData();
		table = new JTable(model);
		table.setDefaultRenderer(Object.class, new MyTableCellRenderer());
		table.setShowHorizontalLines(false);
		table.setShowVerticalLines(false);

		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(2).setPreferredWidth(150);
		table.getColumnModel().getColumn(3).setPreferredWidth(150);
		table.getColumnModel().getColumn(4).setPreferredWidth(150);
		table.getColumnModel().getColumn(5).setPreferredWidth(150);
		
		
		this.setLayout(new BorderLayout());
		this.configureLowerPanel();
		this.configureTableListeners();
		this.addUnitListeners();
		eventCodeBox.addRelationshipListener(eventNameField);
		
		JScrollPane sp = new JScrollPane(table);
		sp.setPreferredSize(new Dimension(500, 500));
		this.add(sp, BorderLayout.CENTER);			
		this.add(lowerPanel, BorderLayout.PAGE_END);
	}
	
	public void configureLowerPanel() {
		lowerPanel = new JPanel(new BorderLayout());
		lowerPanel.setBorder(BorderFactory.createTitledBorder("EDIT TABLE"));
		gridPanel = new JPanel(new GridLayout(4, 4));
		
		JLabel eventCodeLabel = new JLabel(Event.EVENT_CODE);
		eventCodeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel eventNameLabel = new JLabel(Event.EVENT_NAME);
		eventNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel scoreUnitLabel = new JLabel(Event.SCORE_UNIT);
		scoreUnitLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel scoreMaxLabel = new JLabel(Event.SCORE_MAX);
		scoreMaxLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel scoreMinLabel = new JLabel(Event.SCORE_MIN);
		scoreMinLabel.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel sortSeqLabel = new JLabel(Event.SORT_SEQ);
		sortSeqLabel.setHorizontalAlignment(SwingConstants.CENTER);

		eventNameField = new JTextField("");
		scoreMinField = new ScorePanel(); 
		scoreMaxField = new ScorePanel();
		sortSeqField = new JTextField("");
		eventCodeBox = EventComboBox.getInstance();
		
		scoreBox = new JComboBox (ScorePanel.scoreTypes);
		scoreBox.setSelectedIndex(-1);
		
		save = new JButton("Save");
		save.setBackground(Color.BLACK);
		save.setForeground(Color.WHITE);
		save.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				editEvent();
			}	
		});
		
		delete = new JButton("Delete");
		delete.setBackground(new Color(110, 0, 0));
		delete.setForeground(Color.WHITE);
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					controller.deleteSelectedEvents();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			}
		});
		
		gridPanel.add(scoreUnitLabel);
		gridPanel.add(sortSeqLabel);
		gridPanel.add(eventNameLabel);
		gridPanel.add(eventCodeLabel);
		gridPanel.add(scoreBox);
		gridPanel.add(sortSeqField);
		gridPanel.add(eventNameField);
		gridPanel.add(eventCodeBox);
		
		gridPanel.add(scoreMinLabel);
		gridPanel.add(scoreMaxLabel);
		gridPanel.add(new JLabel(""));
		gridPanel.add(delete);
		
		gridPanel.add(scoreMinField);
		gridPanel.add(scoreMaxField);
		gridPanel.add(new JLabel(""));
		gridPanel.add(save);
		
		lowerPanel.add(gridPanel, BorderLayout.CENTER);
	}
	
	
	
	public void configureTableListeners() {
		/* Populates fields underneath table whenever a row is selected */
		table.getSelectionModel().addListSelectionListener
		(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				controller.populateMaintenanceFields();
			}
		});
	
		/* Deletes rows whenever the Delete key is pressed */
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					try {
						controller.deleteSelectedEvents();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
	}
		
	public void editEvent() {
		String errMsg = "Error: Invalid Data Entered";
		ErrDialog dia = new ErrDialog(errMsg);
		dia.setPreferredSize(new Dimension(200, 100));
		
		try {
			controller.updateHeatParams();
		} catch (Exception ex) { 
			String msg = ex.getLocalizedMessage();
			if (msg == null) return;
			
			if (msg.contains("\"")) {
				msg = msg.substring(msg.indexOf("\""));
				msg = msg.replaceAll("\"", "");
			}
			
			if (msg.equals(ErrDialog.NAME_DNE_ERROR)) {
				errMsg = "Error: event name must not be left blank."; 
			}
			else if (msg.equals(ErrDialog.SCORE_WF_ERROR)) {
				errMsg = "Error: score must be input as numbers."
						+ " Additionally, if Distance is selected, "
						+ " inches must not exceed 11. If Time is"
						+ " selected, seconds must not exceed 59.";
			}
			else if (msg.equals(ErrDialog.SCORE_RANGE_ERROR)) {
				errMsg = "Error: min score must be less than max"
						+ " score and vice versa.";
			}
			else if (msg.equals(ErrDialog.SORT_DNE_ERROR)) {
				errMsg = "Sort sequence field must not be left blank.";
			}
			else if (msg.equals(ErrDialog.SORT_WF_ERROR)) {
				errMsg = "Sort sequence must be a number.";
			}
								
			dia.setDialog(errMsg);
			dia.setVisible(true);
		}
	}
	
	public void addUnitListeners() {
		eventCodeBox.addUnitListener(scoreMinField);
		eventCodeBox.addUnitListener(scoreMaxField);
		scoreBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (scoreBox.getItemCount() > 0 && scoreBox.getSelectedIndex() > -1) {		
					final String unit = scoreBox.getSelectedItem().toString();
					
					if (unit.equals(ScorePanel.T)) {
						scoreMinField.setEnabled(true);
						scoreMaxField.setEnabled(true);
						scoreMinField.setToTimeUnits();
						scoreMaxField.setToTimeUnits();
					}
					else if (unit.equals(ScorePanel.D)) {
						scoreMinField.setEnabled(true);
						scoreMaxField.setEnabled(true);
						scoreMinField.setToDistanceUnits();
						scoreMaxField.setToDistanceUnits();
					}
					else if (unit.equals(ScorePanel.N)) {
						scoreMinField.clearText();
						scoreMaxField.clearText();
						scoreMinField.clearUnits();
						scoreMaxField.clearUnits();
						scoreMinField.setEnabled(false);
						scoreMaxField.setEnabled(false);
					}
				}
				
				else if (scoreBox.getSelectedIndex() == -1) {
					scoreMinField.clearUnits();
					scoreMaxField.clearUnits();
				}
			}
		});
	}
	
	public static JTable getTable() {
		return table;
	}
	public static EventTableModel getModel() {
		return model;
	}

	public static JTextField getEventNameField() {
		return eventNameField;
	}

	public static JTextField getSortSeqField() {
		return sortSeqField;
	}

	public static ScorePanel getScoreMinField() {
		return scoreMinField;
	}

	public static ScorePanel getScoreMaxField() {
		return scoreMaxField;
	}

	public static EventComboBox getEventCodeBox() {
		return eventCodeBox;
	}

	public  static JComboBox getScoreBox() {
		return scoreBox;
	}
}
