package ui;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

public class MySet <T> extends TreeSet <T> {

	private static final long serialVersionUID = -8237397273577514601L;

	public MySet (Comparator <T> comparator) {
		super(comparator);
	}
	
	public T get(final int index) {
		int count = 0;
		T temp = null;
		for (Iterator<T> it = this.iterator(); it.hasNext(); count++) {
	        temp = it.next();
	        if (count == index) break;
		}
		
		return temp;
	}
	
	public int getIndexOf(final T arg0) {
		int count = 0;
		T temp = null;
		for (Iterator<T> it = this.iterator(); it.hasNext(); count++) {
	        temp = it.next();
	        if (temp.equals(arg0)) break;
		}
		
		return count;
	}
}
