package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.EventCreateController;

public class EventCreatePanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JPanel top, bottom;
	private JButton addButton;
	private static JTextField eventNameField, eventCodeField; 
	private static ScorePanel scoreMaxField, scoreMinField;
	private static JComboBox scoreBox, sortSeqBox;
	
	private EventCreateController controller = new EventCreateController();
	
	public EventCreatePanel() {
		this.setLayout(new BorderLayout());
		JPanel buttons = new JPanel(new GridLayout(0, 2));
		top = new JPanel(new BorderLayout());
		top.setBorder(BorderFactory.createTitledBorder("ADD NEW EVENT"));
		bottom = new JPanel(new BorderLayout());
		
		/* Function adds appropriate action listeners to add/remove buttons */
		this.configureButtons();
		buttons.add(addButton);
		
		/* JLabel and JTextField for each appropriate field relating to Event 
		 * class */
		final JLabel eventCodeLabel= new JLabel("Event Code");
		final JLabel eventNameLabel= new JLabel("Event Name");
		final JLabel scoreUnitLabel = new JLabel("Score Unit");
		final JLabel scoreMaxLabel = new JLabel("Max Score");
		final JLabel scoreMinLabel = new JLabel("Min Score");
		final JLabel sortSeqLabel = new JLabel("Sort Sequence");
		eventNameField = new JTextField("");
		scoreMaxField = new ScorePanel();
		scoreMinField = new ScorePanel();
		eventCodeField = new JTextField("");
		
		/* Configure ComboBoxes */
		scoreBox = new JComboBox (ScorePanel.scoreTypes);
		scoreBox.setSelectedIndex(-1);
		
		sortSeqBox = new JComboBox();
		sortSeqBox.addItem(new Integer(10));
		sortSeqBox.setSelectedIndex(-1);
		this.addUnitListener();
		
		final JPanel modiPanel = new JPanel();
		final GroupLayout groupLayout = new GroupLayout(modiPanel);
		modiPanel.setLayout(groupLayout);
		groupLayout.setAutoCreateGaps(true);
		groupLayout.setAutoCreateContainerGaps(true);	
		
		groupLayout.setHorizontalGroup(
			groupLayout.createSequentialGroup()
		   		.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
	   				.addComponent(eventCodeLabel)
	   				.addComponent(eventNameLabel)
	   				.addComponent(scoreUnitLabel)
	   				.addComponent(scoreMinLabel)
	   				.addComponent(scoreMaxLabel)
	   				.addComponent(sortSeqLabel))
	   			.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
   					.addComponent(eventCodeField)
	   				.addComponent(eventNameField)
	   				.addComponent(scoreBox)
	   				.addComponent(scoreMinField)
	   				.addComponent(scoreMaxField)
	   				.addComponent(sortSeqBox))
		);

		groupLayout.setVerticalGroup(
		   groupLayout.createSequentialGroup()
		   	   .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(eventCodeLabel)
		           .addComponent(eventCodeField))
		       .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(eventNameLabel)
		           .addComponent(eventNameField))
	           .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(scoreUnitLabel)
		           .addComponent(scoreBox))
	           .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
        		   .addComponent(scoreMinLabel)
		           .addComponent(scoreMinField))
		      .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
		           .addComponent(scoreMaxLabel)
		           .addComponent(scoreMaxField))
	          .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(sortSeqLabel)
		           .addComponent(sortSeqBox))
		);
		
		top.add(modiPanel, BorderLayout.CENTER);
		top.add(buttons, BorderLayout.PAGE_END);
		
		JLabel comment = new JLabel("COMMENTS");
		comment.setFont(new Font("Serif", Font.BOLD, 14));
		
		JTextArea commentBox = new JTextArea("");
		commentBox.setEditable(true);
		commentBox.setBorder(
				BorderFactory.createLineBorder(new Color(99, 150, 225), 1));
		commentBox.setFont(new Font("Serif", Font.PLAIN, 14));
		commentBox.setLineWrap(true);
		commentBox.setWrapStyleWord(true);
		JScrollPane sp = new JScrollPane(commentBox);
		sp.setPreferredSize(new Dimension(100, 100));
		sp.setBorder(BorderFactory.createTitledBorder("COMMENTS"));

		bottom.add(sp, BorderLayout.CENTER);	
		this.add(top, BorderLayout.CENTER);
		this.add(bottom, BorderLayout.PAGE_END);
	}
	
	/* Configure add and delete buttons */ 
	public void configureButtons() {
		addButton = new JButton("Add");
		addButton.setBackground(new Color(0, 100, 0));
		addButton.setForeground(Color.WHITE);
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				String errMsg = "Error: Invalid Data Entered";
				ErrDialog dia = new ErrDialog(errMsg);
				dia.setPreferredSize(new Dimension(200, 100));
				
				try {
					controller.createNewEvent();
				} catch(Exception ex) {
					String msg = ex.getLocalizedMessage();
					if (msg == null) return;
					
					if (msg.contains("\"")) {
						msg = msg.substring(msg.indexOf("\""));
						msg = msg.replaceAll("\"", "");
					}
					
					if (msg.equals(ErrDialog.EVENTCD_NULL_ERROR)) {
						errMsg = "Error: event code must not be left blank.";
					}
					
					else if (msg.equals(ErrDialog.EVENTCD_WF_ERROR)) {
						errMsg = "Error: event code has incorrect format." +
								" Format must be EXXX where X is 0 - 9";
					}
					else if (msg.equals(ErrDialog.NAME_DNE_ERROR)) {
						errMsg = "Error: event name must not be left blank."; 
					}
					else if (msg.equals(ErrDialog.SCORE_WF_ERROR)) {
						errMsg = "Error: score must be input as numbers."
								+ " Additionally, if Distance is selected, "
								+ " inches must not exceed 11. If Time is"
								+ " selected, seconds must not exceed 59.";
					}
					else if (msg.equals(ErrDialog.SCORE_RANGE_ERROR)) {
						errMsg = "Error: min score must be less than max"
								+ " score and vice versa.";
					}
										
					dia.setDialog(errMsg);
					dia.setVisible(true);
				}
			}
		});
	}
	
	public void addUnitListener() {
		scoreBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (scoreBox.getItemCount() > 0 && scoreBox.getSelectedIndex() > -1) {		
					final String unit = scoreBox.getSelectedItem().toString();
					
					if (unit.equals(ScorePanel.T)) {
						scoreMinField.setEnabled(true);
						scoreMaxField.setEnabled(true);
						scoreMinField.setToTimeUnits();
						scoreMaxField.setToTimeUnits();
					}
					else if (unit.equals(ScorePanel.D)) {
						scoreMinField.setEnabled(true);
						scoreMaxField.setEnabled(true);
						scoreMinField.setToDistanceUnits();
						scoreMaxField.setToDistanceUnits();
					}
					else if (unit.equals(ScorePanel.N)) {
						scoreMinField.clearText();
						scoreMaxField.clearText();
						scoreMinField.clearUnits();
						scoreMaxField.clearUnits();
						scoreMinField.setEnabled(false);
						scoreMaxField.setEnabled(false);
					}
				}
				
				else if (scoreBox.getSelectedIndex() == -1) {
					scoreMinField.clearUnits();
					scoreMaxField.clearUnits();
				}
			}
		});
	}
	
	public static JTextField getNameField() {
		return eventNameField;
	}
	public static ScorePanel getScoreMaxField() {
		return scoreMaxField;
	}
	public static ScorePanel getScoreMinField() {
		return scoreMinField;
	}
	public static JTextField getCodeField() {
		return eventCodeField;
	}
	public static JComboBox getsortSeqBox() {
		return sortSeqBox;
	}
	public static JComboBox getScoreBox() {
		return scoreBox;
	}
}

