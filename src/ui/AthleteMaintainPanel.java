package ui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.table.AbstractTableModel;

import controller.AthleteMaintainController;
import datastore.DataStore;
import domain.Athlete;
import domain.School;
import domain.Teacher;

/* Maintain panels are responsible for embedding the table they correspond to.
 * They also provide functionality for editing user-defined data within the table */
public class AthleteMaintainPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private static final String[] columnNames = {"First Name", "Last Name", 
		"Gender", "Age", "School Name", "Score", "Event #", "Event Name", 
		"Group #", "Group Leader"};
	
	private AthleteTableModel model;
	private JTable table = new JTable(model);
	
	private JPanel gridPanel, lowerPanel;
	private JTextField firstField, lastField, ageField;
	private EventComboBox eventCodeBox;
	private JComboBox genderBox, grpBox, schoolBox, teacherBox;
	private ScorePanel scoreField;
	private JButton save, delete;
	
	final private AthleteMaintainController controller = 
			new AthleteMaintainController();

	public AthleteMaintainPanel() {
		model = new AthleteTableModel(columnNames);
		model.popData();
		table = new JTable(model);
		
		/* Overrides the JTable's mechanical look with something that looks
		 * more like the iTunes table background */
		table.setDefaultRenderer(Object.class, new MyTableCellRenderer());
		table.setShowHorizontalLines(false);
		table.setShowVerticalLines(false);
		
		/* Whenever a table header column is selected, 
		 * it is sorted accordingly */
		table.getTableHeader().addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent e) {
		        Athlete cur = null;
		        int newRow = 0;
		        
		        if (table.getSelectedRow() > 0) {
		        	cur = model.getAthlete(table.getSelectedRow());
		        }
		        
		        int col = table.columnAtPoint(e.getPoint());
		        model.switchSortTypeTo(col, true);
		        
		        if (cur != null)
		        	newRow = model.getAthleteList().indexOf(cur);
		        table.getSelectionModel().setSelectionInterval(newRow, newRow);
				table.scrollRectToVisible(new Rectangle(table.getCellRect(newRow, 0, true)));
		    }
		});
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0).setPreferredWidth(90);
		table.getColumnModel().getColumn(1).setPreferredWidth(90);
		table.getColumnModel().getColumn(2).setPreferredWidth(65);
		table.getColumnModel().getColumn(3).setPreferredWidth(35);
		table.getColumnModel().getColumn(4).setPreferredWidth(120);
		table.getColumnModel().getColumn(6).setPreferredWidth(90);
		table.getColumnModel().getColumn(7).setPreferredWidth(100);
		table.getColumnModel().getColumn(8).setPreferredWidth(70);
		table.getColumnModel().getColumn(9).setPreferredWidth(91);
		
		this.setLayout(new BorderLayout());
		this.configureLowerPanel();
		this.configureTableListeners();
		
		JScrollPane sp = new JScrollPane(table);
		sp.setPreferredSize(new Dimension(500, 500));
		this.add(sp, BorderLayout.CENTER);			
		this.add(lowerPanel, BorderLayout.PAGE_END);
	}
	
	public void configureLowerPanel() {
		lowerPanel = new JPanel(new BorderLayout());
		lowerPanel.setBorder(BorderFactory.createTitledBorder("EDIT TABLE"));
		gridPanel = new JPanel(new GridLayout(4, 5));
		
		JLabel first = new JLabel(Athlete.FIRST_NAME);
		first.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel last = new JLabel(Athlete.LAST_NAME);
		last.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel age = new JLabel(Athlete.AGE);
		age.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel gender = new JLabel(Athlete.GENDER);
		gender.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel school = new JLabel(Athlete.SCHOOL);
		school.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel teacher = new JLabel("Teacher");
		teacher.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel score = new JLabel(Athlete.SCORE);
		score.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel groupCode = new JLabel("Grp Code");
		groupCode.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel groupLeader = new JLabel("Grp Leader");
		groupLeader.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel eventCode = new JLabel("Event Code");
		eventCode.setHorizontalAlignment(SwingConstants.CENTER);
		
		firstField = new JTextField("");
		lastField = new JTextField("");
		ageField = new JTextField("");
		scoreField = new ScorePanel();
		
		eventCodeBox = EventComboBox.getInstance();
		genderBox = new JComboBox (Athlete.GENDERS);
		genderBox.setSelectedIndex(-1);
		grpBox = new JComboBox (Athlete.GRP_CODES);
		grpBox.setSelectedIndex(-1);
		schoolBox = new JComboBox();
		teacherBox = new JComboBox();
		
		/* Load school, teacher and group data */
		DataStore.getInstance().loadComboBoxData();
		
		/* Listener that triggers unit label changes */
		eventCodeBox.addUnitListener(scoreField);
		
		/* Populate school combo box and group combo box */
		this.populateSchoolBox(School.getListOfSchoolNames());
		this.populateGroupBox(Teacher.getGroupCodes());
		
		/* Associate school name with teacher names that are populated inside 
		 * teacherBox */
		schoolBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.populateTeacherBox(schoolBox, teacherBox);
			}
		});
		
		save = new JButton("Save");
		save.setBackground(Color.BLACK);
		save.setForeground(Color.WHITE);
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveNewAthlete();
			}
		});
		
		delete = new JButton("Delete");
		delete.setBackground(new Color(110, 0, 0));
		delete.setForeground(Color.WHITE);
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					controller.deleteSelectedAthletes();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        }
		});
		
		gridPanel.add(first);
		gridPanel.add(last);
		gridPanel.add(gender);
		gridPanel.add(age);
		gridPanel.add(eventCode);
		gridPanel.add(firstField);
		gridPanel.add(lastField);
		gridPanel.add(genderBox);
		gridPanel.add(ageField);
		gridPanel.add(eventCodeBox);
		gridPanel.add(school);
		gridPanel.add(teacher);
		gridPanel.add(score);
		gridPanel.add(groupCode);
		gridPanel.add(delete);
		gridPanel.add(schoolBox);
		gridPanel.add(teacherBox);
		gridPanel.add(scoreField);
		gridPanel.add(grpBox);
		gridPanel.add(save);
		
		lowerPanel.add(gridPanel, BorderLayout.CENTER);
	}
	
	public void configureTableListeners() {
		/* Populates fields underneath table whenever a row is selected */
		table.getSelectionModel().addListSelectionListener
		(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				controller.populateMaintenanceFields(AthleteMaintainPanel.this);
			}
		});
	
		/* Deletes rows whenever the Delete key is pressed */
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					try {
						controller.deleteSelectedAthletes();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
	}
	
	public void saveNewAthlete() {
		String errMsg = "Error: Invalid Data Entered";
		ErrDialog errDialog = new ErrDialog(errMsg);

		try {
			controller.updateAthleteParams(this);
		} catch(Exception ex) {
String msg = ex.getLocalizedMessage();
			
			if (msg.contains("\"")) {
				msg = msg.substring(msg.indexOf("\""));
				msg = msg.replaceAll("\"", "");
			}
			
			if (msg.equals(ErrDialog.AGE_WF_ERROR) || 
					msg.equals(ErrDialog.AGE_RANGE_ERROR)) {
				errMsg = "Error: age must be a number between 6 - 99.";
			}
			else if (msg.equals(ErrDialog.AGE_RANGE_ERROR)) {
				errMsg = "Error: Athlete's age must be within the range "
						+ Athlete.MIN_AGE + " - " + Athlete.MAX_AGE + ".";
			}
			else if (msg.equals(scoreField.getText())) {
				errMsg = "Error: invalid Score. "
						+ "Must be a number.";
			}
			else if (msg.equals(ErrDialog.EVENTCD_NULL_ERROR)) {
				errMsg = "Error: Event Code does not exist. First create the"
						+ " event code in the Event Panel.";
				new WarningTimer(NavMenu.events);
			}
			else if (msg.equals(ErrDialog.SCHOOL_NS_ERROR)) {
				errMsg = ErrDialog.SCHOOL_NS_ERROR;
			}
			else if (msg.equals(ErrDialog.TEACHER_NS_ERROR)) {
				errMsg = ErrDialog.TEACHER_NS_ERROR;
			}
			else if (msg.contains(ErrDialog.SCORE_RANGE_ERROR)) {
				errMsg = msg;
			}
			else if (msg.equals(ErrDialog.ATH_REG_ERROR1)) {
				errMsg = "Error: Athlete cannot be registered for same event"
						+ " more than once.";
			}
			else if (msg.equals(ErrDialog.ATH_REG_ERROR2)) {
				errMsg = "Error: Athlete cannot be registered for"
						+ " more than two events.";
			}
			
			errDialog.setDialog(errMsg);
			errDialog.setVisible(true);
		}
	}
	
	private void populateGroupBox(Vector <String> groups) {
		grpBox.setModel(new DefaultComboBoxModel(groups));
		schoolBox.setSelectedIndex(-1);
	}
	
	private void populateSchoolBox(ArrayList <String> schools) {
		schoolBox.setModel(new DefaultComboBoxModel(schools.toArray()));
		schoolBox.setSelectedIndex(-1);
	}
	
	public JTable getTable() {
		return table;
	}
	public AthleteTableModel getModel() {
		return model;
	}	
	public JTextField getFirstNameField() {
		return firstField;
	}
	public JTextField getLastNameField() {
		return lastField;
	}
	public JComboBox getSchoolDropDown() {
		return schoolBox;
	}
	public JComboBox getTeacherDropDown() {
		return teacherBox;
	}
	public JTextField getAgeField() {
		return ageField;
	}
	public ScorePanel getScoreField() {
		return scoreField;
	}
	public JComboBox getCodeBox() {
		return eventCodeBox;
	}
	public JComboBox getGenderDropDown() {
		return genderBox;
	}
	public JComboBox getGrpDropDown() {
		return grpBox;
	}
}
