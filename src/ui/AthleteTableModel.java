package ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import domain.Athlete;
import datastore.DataStore;

public class AthleteTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private ArrayList <Athlete> list;
	private static DataStore dat;
	private String[] columnNames;
	
	private int prevRowHeader;
	
	public AthleteTableModel(String[] columnNames) {
		this.columnNames = columnNames;
	}
	
	public void switchSortTypeTo(final int rowHeader, boolean again) {
		if (prevRowHeader == rowHeader && again)
			Athlete.invertComparatorSign();
		else
			Athlete.resetComparatorSign();
		
		switch(rowHeader) {
			case 0:
				Collections.sort(list, Athlete.compFirstName);
				break;
			case 1:
				Collections.sort(list, Athlete.compFullName);
				break;
			case 3:
				Collections.sort(list, Athlete.compAge);
				break;
			case 4:
				Collections.sort(list, Athlete.compSchool);
				break;
			case 5:
				Collections.sort(list, Athlete.compScore);
				break;
			case 6:
				Collections.sort(list, Athlete.compEventCode);
				break;
		}
		/* Keep track of the previous row header clicked; if the same 
		 * one is clicked twice, invert sort sequence */
		prevRowHeader = rowHeader;
	}
	
	public void popData() {
		dat = DataStore.getInstance();
		list = new ArrayList <Athlete> (dat.getAthletes());
		/* Initialize prevRowHeader to match initial column sort type */
		Collections.sort(list, Athlete.compFullName);
		prevRowHeader = 1;
	}
	
	public void saveData() throws IOException {
		dat.saveAllData();
	}
	
	public int getRowCount() {
		return list.size();
	}

	public int getColumnCount() {
		return columnNames.length;
	}
	
	public String getColumnName(int col) {
        return columnNames[col];
    }
	
	public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

	public String getValueAt(int row, int col) {
        return list.get(row).getColumn(col);
    }
	
	public void setValueAt(String val, int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				list.get(rowIndex).setFirstName(val);
				break;
			case 1:
				list.get(rowIndex).setLastName(val);
				break;
			case 2:
				list.get(rowIndex).setGender(val);
				break;
			case 3:
				list.get(rowIndex).setAge(val);
				break;
			case 4:
				list.get(rowIndex).setSchool(val);
				break;
			case 5:
				list.get(rowIndex).setScore(val);
				break;
			case 6:
				list.get(rowIndex).setEventCode(val);
				break;
			case 7:
				list.get(rowIndex).setEventName(val);
				break;
			case 8:
				list.get(rowIndex).setGrpCode(val);
				break;
			case 9:
				list.get(rowIndex).setGrpLeader(val);
				break;
		}
	}
	
	public int addAthlete(Athlete a) throws Exception {
		list.add(a);
		dat.addAthlete(a);
		
		/* Sort the table data after inserting new athlete
		 * and return the new row of the athlete post-sort */
		int prevRow = prevRowHeader;
		if (prevRow == -1) prevRow = 0;
		this.switchSortTypeTo(prevRow, false);
		return list.indexOf(a);
	}
	
	public int addAthlete(Athlete a, int row) throws Exception {
		list.add(row, a);
		dat.addAthlete(a);
		
		/* Sort the table data after inserting new athlete
		 * and return the new row of the athlete post-sort */
		int prevRow = prevRowHeader;
		if (prevRow == -1) prevRow = 0;
		this.switchSortTypeTo(prevRow, false);
		return list.indexOf(a);
	}

	public void deleteAthlete(int row) throws Exception {
		dat.removeAthlete(list.get(row));
		list.remove(row);
	}
	
	public boolean isCellEditable(int row, int col) { 
		return list.get(row).getEditable();
    }
	
	public void setCellEditable(boolean change, int row) {
		list.get(row).setEditable(change);
	}
	
	public Athlete getAthlete(int row ) {
		return list.get(row);
	}
	
	public ArrayList <Athlete> getAthleteList() {
		return list;
	}
	
	/* Used by the athlete create controller: if 2 athlete's identical to
	 * newAthlete already exist, then prevent the 3rd event registration 
	 */
	public int checkForRedundancies(Athlete newAthlete) {
		int count = 0;
		
		final String code1 = newAthlete.getEventCode();
		final String key1  = newAthlete.generateUniqueKey();
		for (int i = 0; i < list.size(); i++) {
			String code2 = this.getAthlete(i).getEventCode(); 
			String key2  = this.getAthlete(i).generateUniqueKey();
			
			if (key1.equals(key2)) {
				if (code1.equals(code2))
					return -1;
				
				count++;
				if (count == 2)
					return -2;
			}
		}	
		
		return 1;
	}
	
	/* Used by the athlete maintain controller: if 2 athlete's identical to
	 * newAthlete exist AND neither table selection corresponds to one of those
	 * athletes, then the user is attempting to edit another athlete in order
	 * to register the same athlete for a 3rd time; prevent this from happening
	 */
	public int checkForRedundancies(JTable table, Athlete newAthlete) {
		int count1 = 0, count2 = 0;
		Vector <Integer> rows = new Vector <Integer> ();
		
		final String code1 = newAthlete.getEventCode();
		final String key1  = newAthlete.generateUniqueKey();
		for (int i = 0; i < list.size(); i++) {
			String code2 = this.getAthlete(i).getEventCode(); 
			String key2  = this.getAthlete(i).generateUniqueKey();
			
			if (key1.equals(key2)) {
				if (code1.equals(code2))
					return -1;
				
				count1++;
				rows.add(i);
			}
		}

		for (int i = 0; i < rows.size(); i++) {
			if (rows.get(i) != table.getSelectedRow())
				count2++;
		}
	
		if (count1 == 2 && count2 == 2)
			return -2;
		
		return 1;
	}
}
