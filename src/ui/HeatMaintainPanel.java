package ui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import controller.HeatMaintainController;
import domain.Heat;

public class HeatMaintainPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private static final String[] columnNames = {"Event Code", "Event Name", 
		"Gender", "Min Age", "Max Age", "Time", "# Heats"};
	
	private static HeatTableModel model = new HeatTableModel(columnNames);
	private static JTable table = new JTable(model);
	private JPanel gridPanel, lowerPanel;
	
	private static JTextField eventNameField, numHeatsField, minAgeField,
		maxAgeField;
	private static ScorePanel timeField;
	private static EventComboBox eventCodeBox;
	private static JComboBox genderBox;
	private JButton save, delete;
	
	private HeatMaintainController controller = new HeatMaintainController();
	
	public HeatMaintainPanel() {
		model.popData();
		table = new JTable(model);
		table.setDefaultRenderer(Object.class, new MyTableCellRenderer());
		table.setShowHorizontalLines(false);
		table.setShowVerticalLines(false);
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(2).setPreferredWidth(150);
		table.getColumnModel().getColumn(3).setPreferredWidth(150);
		table.getColumnModel().getColumn(4).setPreferredWidth(150);
		table.getColumnModel().getColumn(5).setPreferredWidth(150);
		table.getColumnModel().getColumn(6).setPreferredWidth(150);
		
		this.setLayout(new BorderLayout());
		this.configureLowerPanel();
		this.configureTableListeners();
		
		this.add(new JScrollPane(table), BorderLayout.CENTER);			
		this.add(lowerPanel, BorderLayout.PAGE_END);
	}
	
	public void configureLowerPanel() {
		lowerPanel = new JPanel(new BorderLayout());
		lowerPanel.setBorder(BorderFactory.createTitledBorder("EDIT TABLE"));
		gridPanel = new JPanel(new GridLayout(4, 5));
		
		JLabel eventCode = new JLabel(Heat.EVENT_CODE);
		eventCode.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel eventName = new JLabel(Heat.EVENT_NAME);
		eventName.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel gender = new JLabel(Heat.GENDER);
		gender.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel minAge = new JLabel(Heat.MIN_AGE);
		minAge.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel maxAge = new JLabel(Heat.MAX_AGE);
		maxAge.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel time = new JLabel(Heat.TIME);
		time.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel numHeats = new JLabel(Heat.NUM_HEATS);
		numHeats.setHorizontalAlignment(SwingConstants.CENTER);
		
		eventNameField = new JTextField("");
		minAgeField = new JTextField(""); 
		maxAgeField = new JTextField("");
		timeField = new ScorePanel();
		numHeatsField = new JTextField("");
		eventCodeBox = EventComboBox.getInstance();
		eventCodeBox.addUnitListener(timeField);
		genderBox = new JComboBox (Heat.GENDER_OPTIONS);
		genderBox.setSelectedIndex(-1);
		
		/* Associate event name with event code via action listener */
		eventCodeBox.addRelationshipListener(eventNameField);
		/* Listener that triggers unit label changes */
		eventCodeBox.addUnitListener(timeField);
		
		save = new JButton("Save");
		save.setBackground(Color.BLACK);
		save.setForeground(Color.WHITE);
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveNewHeat();	
			}
			
		});
		
		delete = new JButton("Delete");
		delete.setBackground(new Color(110, 0, 0));
		delete.setForeground(Color.WHITE);
		delete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					deleteHeat();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		});
		
	
		
		gridPanel.add(gender);
		gridPanel.add(minAge);
		gridPanel.add(maxAge);
		gridPanel.add(eventName);
		gridPanel.add(eventCode);
		
		gridPanel.add(genderBox);
		gridPanel.add(minAgeField);
		gridPanel.add(maxAgeField);
		gridPanel.add(eventNameField);
		gridPanel.add(eventCodeBox);
		
		gridPanel.add(time);
		gridPanel.add(numHeats);
		gridPanel.add(new JLabel(""));
		gridPanel.add(new JLabel(""));
		gridPanel.add(delete);
		gridPanel.add(timeField);
		gridPanel.add(numHeatsField);
		gridPanel.add(new JLabel(""));
		gridPanel.add(new JLabel(""));
		gridPanel.add(save);
		
		lowerPanel.add(gridPanel, BorderLayout.CENTER);
	}
	
	public void saveNewHeat(){
		try {
			controller.updateHeatParams();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void configureTableListeners(){

		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				controller.populateMaintenanceFields();
			}
		});
	}

	public void deleteHeat() throws Exception{
		controller.deleteSelectedHeats();
	}
	
	public static JTable getTable() {
		return table;
	}
	
	public static HeatTableModel getModel() {
		return model;
	}
	
	public static JTextField getMinAgeField() {
		return minAgeField;
	}
	public static JTextField getMaxAgeField() {
		return maxAgeField;
	}
	public static ScorePanel getTimeField() {
		return timeField;
	}
	public static JComboBox getEventCodeBox() {
		return eventCodeBox;
	}
	public static JComboBox getGenderBox() {
		return genderBox;
	}
	public static JTextField getEventNameField() {
		return eventNameField;
	}
	public static JTextField getNumHeatsField() {
		return numHeatsField;
	}
}

