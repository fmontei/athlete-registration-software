package ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ScorePanel extends JPanel {
	
	private static final long serialVersionUID = -3902427337536734901L;
	public static final String N = "N";
	public static final String D = "D";
	public static final String T = "T";
	public static final float NOT_AVAILABLE = -1.0f;
	protected static final String[] scoreTypes = {N, D, T};
	
	private JTextField f1, f2;
	private JLabel u1 = new JLabel("  ");
	private JLabel u2 = new JLabel("  ");
	
	public ScorePanel() {	
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
	
		f1 = new JTextField(4);
		f2 = new JTextField(2);
		limitCharacters(f1, 4);
		limitCharacters(f2, 2);

		this.add(f1);
		this.add(u1);
		this.add(f2);
		this.add(u2);

		this.setSize(new Dimension(200, 20));
		this.setMaximumSize(new Dimension(200, 20));
		this.setVisible(true);
	}
	
	public void clearText() {
		f1.setText("");
		f2.setText("");
	}
	
	public void setText(final String entry) {
		if (entry.contains(".")) {
			f1.setText(entry.substring(0, entry.indexOf(".")));
			f2.setText(entry.substring(entry.indexOf(".") + 1, entry.length()));
		}
		else {
			f1.setText(entry);
			f2.setText("");
		}
	}
	
	public String getText() {
		String text1 = f1.getText();
		String text2 = f2.getText();
		
		if (text1.equals("") && text2.equals(""))
			return "";
		else
			return text1 + "." + text2;
	}
	
	public void limitCharacters(final JTextField tf, final int length) {
	    tf.getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {
				if(tf.getText().length() >= length) {
					 SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {   
							try {
								tf.setText(tf.getText().substring(0, length));
							} catch(Exception e) {}
						}
				    });
				}
			}
			public void removeUpdate(DocumentEvent e) {		
			}
			public void changedUpdate(DocumentEvent e) {		
			}
	    });
	}
	
	public void setToTimeUnits() {
		u1.setText("min");
		u1.repaint();
		u2.setText("sec");
		u2.repaint();
	}
	public void setToDistanceUnits() {
		u1.setText("ft");
		u1.repaint();
		u2.setText("in");
		u2.repaint();
	}
	public void clearUnits() {
		u1.setText("  ");
		u1.repaint();
		u2.setText("  ");
		u2.repaint();
	}
	public void setEnabled(boolean b) {
		if (b == false) {
			clearUnits();
		}
		f1.setEnabled(b);
		f2.setEnabled(b);
	}
}
