package ui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.Timer;

public class WarningTimer {
	private int timerCount = 0;
	private Timer timer;
	
	/* Controls flashing warning background for specific button */
	public WarningTimer(final JButton button) {
		final Color original = button.getBackground();
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				timer.stop();
				button.setBackground(original);
			}
		});
		
		timer = new Timer(500, new ActionListener() {
		    @Override 
		    public void actionPerformed(ActionEvent e) {
		    	timerCount++;
		    	if (timerCount % 2 == 0)
		    		button.setBackground(new Color(120, 158, 228));
		    	else
		    		button.setBackground(new Color(5, 103, 255));
		    }
		});
		timer.start();
	}
}
