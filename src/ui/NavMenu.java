package ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import datastore.DataStore;
import domain.Athlete;
import controller.HeatSheetController;
import controller.HeatSheetGenerator;
import controller.NavController;

public class NavMenu extends JPanel {
	private static final long serialVersionUID = 1L;
	
	protected static final String REGISTRATION = "Registration";
	protected static final String EVENTS = "Events";
	protected static final String HEATS = "Heats";
	protected static final String GROUPS = "Groups";
	protected static final String TEACHERS = "Teachers";
	protected static final String SCHOOLS = "Schools";
	protected static final String HEAT_SHEETS = "Heat Sheets";
	protected static final String GROUP_SHEETS = "Group Sheets";
	protected static final String NAME_TAGS = "Name Tags";
	protected static final String SETTINGS = "Settings";
    
    protected static JButton register, events, heats,groups, schools, teachers, 
		heatSheets, groupSheets, nameTags, settings;
    
    private NavController controller = new NavController();
    
	public NavMenu() {
		
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setPreferredSize(new Dimension(180, 600));
		this.setBorder(BorderFactory.createTitledBorder("NAVIGATION"));
		
		final JPanel panels = new JPanel();
		final GroupLayout layout = new GroupLayout(panels);
		panels.setLayout(layout);
	
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		this.createNavButtons();
	    
		layout.setHorizontalGroup(
			layout.createSequentialGroup()
	   			.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	   				.addComponent(register, 130, 130, 130)
	   				.addComponent(events, 130, 130, 130)
	   				.addComponent(heats, 130, 130, 130)
	   				.addComponent(groups, 130, 130, 130)
	   				.addComponent(teachers, 130, 130, 130)
	   				.addComponent(schools, 130, 130, 130)
	   				.addComponent(groupSheets, 130, 130, 130)
	   				.addComponent(nameTags, 130, 130, 130)
	   				.addComponent(settings, 130, 130, 130)
	   				.addComponent(heats, 130, 130, 130)
	   				.addComponent(heatSheets, 130, 130, 130))
		);
	
		layout.setVerticalGroup(
		   layout.createSequentialGroup()
	           .addComponent(register)
	           .addComponent(events)
	           .addComponent(heats)
	           .addComponent(groups)
			   .addComponent(teachers)
			   .addComponent(schools)
			   .addComponent(groupSheets)
			   .addComponent(nameTags)
			   .addComponent(settings)
			   .addComponent(heatSheets, 50, 50, 50)
		);
	
		this.add(panels, BorderLayout.CENTER);
	}
	
	private void createNavButtons() {
		/* Creates and resizes graphical icons for first three buttons */
		ImageIcon registrationIcon = new ImageIcon("images/register.jpg");
		registrationIcon = controller.resizeIcon(registrationIcon);
		ImageIcon eventsIcon = new ImageIcon("images/events.png");
		eventsIcon = controller.resizeIcon(eventsIcon);
		ImageIcon heatsIcon = new ImageIcon("images/heats.png");
		heatsIcon = controller.resizeIcon(heatsIcon);
		ImageIcon sheetIcon = new ImageIcon("images/sheet.png");
		sheetIcon = controller.resizeIcon(sheetIcon);
		
		/* Every nav button created and configured below: */
		register = new JButton("Registration", registrationIcon);
		register.setHorizontalAlignment(SwingConstants.LEFT);
		register.setFont(new Font("Serif", Font.BOLD, 14));
	    register.setActionCommand("register");
		register.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(REGISTRATION);
	        }
	    });
	
		events = new JButton("Events", eventsIcon);
		events.setHorizontalAlignment(SwingConstants.LEFT);
		events.setFont(new Font("Serif", Font.BOLD, 14));
		events.setActionCommand("events");
		events.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(EVENTS);
	        }
	    });
	    
		heats = new JButton("Heats", heatsIcon);
		heats.setHorizontalAlignment(SwingConstants.LEFT);
		heats.setFont(new Font("Serif", Font.BOLD, 14));
	    heats.setActionCommand("heats");
	    heats.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(HEATS);
	        }
	    });
	    
	    groups = new JButton("Groups");
	    groups.setHorizontalAlignment(SwingConstants.LEFT);
		groups.setFont(new Font("Serif", Font.BOLD, 14));
	    groups.setActionCommand("groups");
	    groups.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(GROUPS);
	        }
	    });
	    
	    teachers = new JButton("Teachers");
	    teachers.setHorizontalAlignment(SwingConstants.LEFT);
		teachers.setFont(new Font("Serif", Font.BOLD, 14)); 
	    teachers.setActionCommand("teachers");
	    teachers.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(TEACHERS);
	        	
	        }
	    });
	    
	    schools = new JButton("Schools");
	    schools.setHorizontalAlignment(SwingConstants.LEFT);
		schools.setFont(new Font("Serif", Font.BOLD, 14));
	    schools.setActionCommand("schools");
	    schools.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(SCHOOLS);
	        }
	    });
	    
	    heatSheets = new JButton(sheetIcon);
	    heatSheets.setText("<html>" + "Generate"+ "<br>" + "Heat Sheets" 
	    		+ "</html>");
	    heatSheets.setHorizontalAlignment(SwingConstants.LEFT);
	    heatSheets.setFont(new Font("Serif", Font.BOLD, 14));
	    heatSheets.setActionCommand("heatSheets");
	    heatSheets.addActionListener(new ActionListener() {
	    	 @Override
		        public void actionPerformed(ActionEvent event)
		        {
	    		 		//Begin Generating Heat Sheets
						HeatSheetGenerator gen = new HeatSheetGenerator();
						gen.handleHeatSheetGeneration();
		        }
	    });
	    
	    groupSheets = new JButton("Group Sheets");
	    groupSheets.setHorizontalAlignment(SwingConstants.LEFT);
	    groupSheets.setFont(new Font("Serif", Font.BOLD, 14));
	    groupSheets.setActionCommand("groupSheets");
	    groupSheets.addActionListener(new ActionListener() {
		    @Override
	        public void actionPerformed(ActionEvent event)
	        {
		    	controller.showCardPanel(GROUP_SHEETS);
	        }
	    });
	    
	    nameTags = new JButton("Name Tags");
	    nameTags.setHorizontalAlignment(SwingConstants.LEFT);
	    nameTags.setFont(new Font("Serif", Font.BOLD, 14));
	    nameTags.setActionCommand("nameTags");
	    nameTags.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(NAME_TAGS);
	        }
	    });
	    
	    settings = new JButton("Settings");
	    settings.setHorizontalAlignment(SwingConstants.LEFT);
	    settings.setFont(new Font("Serif", Font.BOLD, 14));
	    settings.setActionCommand("seetings");
	    settings.addActionListener(new ActionListener()
	    {
	        @Override
	        public void actionPerformed(ActionEvent event)
	        {
	        	controller.showCardPanel(SETTINGS);
	        }
	    });
	}

}