package ui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MyTableCellRenderer extends DefaultTableCellRenderer {
	
	private static final long serialVersionUID = 1L;
	public MyTableCellRenderer () {
		 
		 super();
		 this.setOpaque(true);
	}
	
	public Component getTableCellRendererComponent (JTable table, Object value, 
			boolean isSelected, boolean hasFocus, int row, int column) {
		
		if (isSelected) {
	        setBackground(Color.blue);      
	        setForeground(Color.white);      
		}
		
		else {
			if (row % 2 == 0) {  
		        setForeground(Color.black);          
		        setBackground(new Color(233, 240, 255));              
		    }     
			
		    else  {      
		        setBackground(Color.white);      
		        setForeground(Color.black);      
		    }
		}
		
		setText((value == null) ? "" : value.toString());  
		return (this);
	}
}