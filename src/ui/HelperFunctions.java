package ui;

import java.awt.Image;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;

public class HelperFunctions {
	
	protected static ImageIcon resizeIcon(ImageIcon icon) {
		Image img = icon.getImage() ;  
		Image newimg = img.getScaledInstance(20, 20, Image.SCALE_SMOOTH);  
		return new ImageIcon(newimg);
	}

	protected static String parseTime(String entry) throws Exception {
		
		if (entry.length() != 4 || entry.length() != 5)
			throw new Exception();
		else if (!entry.contains(":"))
			throw new Exception();

		try {
			String[] halves = entry.split(":");
			for (String half : halves) {
				Integer.parseInt(half);
			}
		} catch(Exception e) {
			throw new Exception();
		}
		return entry;
	}
}
