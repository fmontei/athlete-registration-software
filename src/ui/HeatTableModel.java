package ui;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import datastore.DataStore;
import domain.Heat;

public class HeatTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	private Vector <Heat> vec = new Vector <Heat>();
	private String[] columnNames;
	private static DataStore dat;
	public HeatTableModel(String[] columnNames) {
		this.columnNames = columnNames;
	}
	public void popData(){
		dat = DataStore.getInstance();
		vec = new Vector<Heat>(dat.getHeats());
	}
	public Vector<Heat> getData(){
		return vec;
	}
	public int getRowCount() {
		return vec.size();
	}

	public int getColumnCount() {
		return columnNames.length;
	}
	
	public String getColumnName(int col) {
        return columnNames[col];
    }
	
	public Class getColumnClass(int c)
    {
        return getValueAt(0, c).getClass();
    }

	public String getValueAt(int row, int col) {
        return vec.get(row).getColumn(col);
    }
	
	public void setValueAt(String val, int rowIndex, int columnIndex) {
		switch(columnIndex) {
			case 0:
				vec.get(rowIndex).setEventCode(val);
				break;
			case 1:
				vec.get(rowIndex).setEventName(val);
				break;
			case 2:
				vec.get(rowIndex).setGender(val);
				break;
			case 3:
				vec.get(rowIndex).setMinAge(Integer.valueOf(val));
				break;
			case 4:
				vec.get(rowIndex).setMaxAge(Integer.valueOf(val));
			case 5:
				vec.get(rowIndex).setTime(val);
			case 6:
				vec.get(rowIndex).setNumHeats(Integer.valueOf(val));
				break;
		}
		this.fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	public void addHeat(Heat a) throws Exception {
		vec.add(a);
		dat.addHeat(a);
	}

	public void deleteHeat(int row) throws Exception {
		dat.removeHeat(vec.get(row));
		vec.remove(row);
	}
	
	public boolean isCellEditable(int row, int col) { 
		return false;
    }
	
	public Heat getHeat(int row) {
		return vec.get(row);
	}
}