package ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import datastore.DataStore;
import domain.Event;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private JFrame frame = new JFrame("Special Olympics Prototype");
	private JPanel masterPanel;
	
	private static CardLayout cardLayout;
    private static JPanel cardPanel;
    private JSplitPane firstCard, secondCard, thirdCard;
	
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
            	try 
                { 
                    UIManager.setLookAndFeel
                    	("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"); 
                } 
                catch(Exception e) { 
                	
                }
            	
                new MainFrame();
                Event.popEvents();
                Event.popAthletes();
                Event.linkHeats();
            }
        });        
    }
    
	public MainFrame() {
		/* Ensures that loadData in DataStore is only called once */
		DataStore dat = DataStore.getInstance();
		dat.initialize();
		
		/* Sets the main frame's image icon after properly resizing it */
		ImageIcon frameIcon = new ImageIcon("images/frame.jpg");
		frameIcon = HelperFunctions.resizeIcon(frameIcon);
		frame.setIconImage(frameIcon.getImage());
		
		masterPanel = new JPanel(new BorderLayout());
		
		/* Creates the panel responsible for providing a tab-like environment,
		 * allowing for the user to easily cycle through the sub-panels */
		this.createCardPanel();
		/* The ever-present navigation menu works hand-in-hand with the card
		 * panel to allow the user to freely navigate across multiple panels */
		NavMenu navMenu = new NavMenu();
		
		masterPanel.add(navMenu, BorderLayout.WEST);
		masterPanel.add(cardPanel, BorderLayout.CENTER);
		frame.setContentPane(masterPanel);	
		
		frame.setPreferredSize(new Dimension(1100, 600));
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.pack();
		frame.setLocationRelativeTo(null);
		
		/* This method must be invoked after the frame has been "realized"; it
		 * sets the first panel's divider location in an aesthetically 
		 * pleasing way such that the table columns and maintenance panel's
		 * fields can be meaningfully read upon program startup */
		firstCard.setDividerLocation(580);
    }
	
	public void createCardPanel() {
		cardLayout = new CardLayout();
        cardPanel = new JPanel();
        cardPanel.setLayout(cardLayout);
        cardLayout.show(cardPanel, "Master");
        
        /* Sets up athlete maintenance "card" or panel */
        final AthleteMaintainPanel panelWest = new AthleteMaintainPanel();
        final AthleteTableModel athleteTableModel = panelWest.getModel();
        	athleteTableModel.popData();
        final JTable athleteTable = panelWest.getTable();
        
		final JPanel panelEast = 
				new AthleteCreatePanel(athleteTableModel, athleteTable);
		panelWest.setPreferredSize(new Dimension(400, 600));
		panelEast.setPreferredSize(new Dimension(400, 600));
		firstCard = new JSplitPane
				(JSplitPane.HORIZONTAL_SPLIT, panelWest, panelEast);
		firstCard.setPreferredSize(new Dimension(800, 600));
		/* Guarantees that the divider location will be resized when the frame
		 * is maximized or resized */
		firstCard.setResizeWeight(0.9);
		/* Places two arrows inside the divider bar: one that shifts the 
		 * bar all the way to the left and the other all the way to the right */
		firstCard.setOneTouchExpandable(true);
		
		final JPanel panelWest1 = new EventMaintainPanel();
		final JPanel panelEast1 = new EventCreatePanel();
		panelWest1.setPreferredSize(new Dimension(400, 600));
		panelEast1.setPreferredSize(new Dimension(400, 600));
		secondCard = new JSplitPane
				(JSplitPane.HORIZONTAL_SPLIT, panelWest1, panelEast1);
		secondCard.setPreferredSize(new Dimension(800, 600));
		secondCard.setResizeWeight(0.9);
		secondCard.setOneTouchExpandable(true);
		
		final JPanel panelWest2 = new HeatMaintainPanel();
		final JPanel panelEast2 = new HeatCreatePanel();
		panelWest2.setPreferredSize(new Dimension(400, 600));
		panelEast2.setPreferredSize(new Dimension(400, 600));
		thirdCard = new JSplitPane
				(JSplitPane.HORIZONTAL_SPLIT, panelWest2, panelEast2);
		thirdCard.setPreferredSize(new Dimension(800, 600));
		thirdCard.setResizeWeight(0.9);
		thirdCard.setOneTouchExpandable(true);
		
		cardPanel.add(firstCard,    NavMenu.REGISTRATION);
		cardPanel.add(secondCard,   NavMenu.EVENTS);
		cardPanel.add(thirdCard,    NavMenu.HEATS);
		cardPanel.add(new JPanel(), NavMenu.GROUPS);
		cardPanel.add(new JPanel(), NavMenu.TEACHERS);
		cardPanel.add(new JPanel(), NavMenu.SCHOOLS);
		cardPanel.add(new JPanel(), NavMenu.HEAT_SHEETS);
		cardPanel.add(new JPanel(), NavMenu.GROUP_SHEETS);
		cardPanel.add(new JPanel(), NavMenu.NAME_TAGS);
		cardPanel.add(new JPanel(), NavMenu.SETTINGS);
		
		/* Forces the divider locations across all 3 JSplitPanes to be synced 
		 * to that of the first card panel, thus perfectly aligning all 3
		 * divider locations at all times */
		PropertyChangeListener propertyChangeListener = new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent changeEvent) {
			    JSplitPane sourceSplitPane = (JSplitPane) changeEvent.getSource();
			    String propertyName = changeEvent.getPropertyName();
			    if (propertyName.equals(JSplitPane.LAST_DIVIDER_LOCATION_PROPERTY)) {
			    	final int current = sourceSplitPane.getDividerLocation();
			    	secondCard.setDividerLocation(current);
			    	thirdCard.setDividerLocation(current);
			    }
			}
	    };
		    
	    firstCard.addPropertyChangeListener(propertyChangeListener);
	}
	
	public static CardLayout getCardLayout() {
		return cardLayout;
	}
	
	public static JPanel getCardPanel() {
		return cardPanel;
	}
}
