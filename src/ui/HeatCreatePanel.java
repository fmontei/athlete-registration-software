package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.HeatCreateController;
import domain.Heat;

public class HeatCreatePanel extends JPanel {

	private static final int ENTRIES = Heat.FIELDS - 1;
	private static final long serialVersionUID = 1L;
	private JPanel top, bottom;
	private JButton addButton;

    private static JTextField minAgeField;
	private static JTextField maxAgeField;
	private static ScorePanel timeField;
	private static EventComboBox eventCodeBox;
	private static JComboBox  genderBox;
	private static JTextField eventNameField;
	private static JTextField numHeatsField;
	private HeatCreateController createController = new HeatCreateController();
	
	public HeatCreatePanel() {
		this.setLayout(new BorderLayout());
		JPanel buttons = new JPanel(new GridLayout(0, 2));
		
		top = new JPanel(new BorderLayout());
		top.setBorder(BorderFactory.createTitledBorder("ADD NEW HEAT"));
		bottom = new JPanel(new BorderLayout());
		
		/* Function adds appropriate action listeners to add/remove buttons */
		this.configureButtons();
		buttons.add(addButton);
		
		/* JLabel and JTextField for each appropriate field relating to Athlete 
		 * class */
		
		final JLabel minAgeLabel = new JLabel("Min Age");
		final JLabel maxAgeLabel = new JLabel("Max Age");
		final JLabel genderLabel = new JLabel("Gender");
		final JLabel eventNumLabel = new JLabel("Event Code");
		final JLabel eventNameLabel = new JLabel("Event Name");
		final JLabel timeLabel = new JLabel("Time");
		final JLabel numHeatsLabel = new JLabel("# Heats");
		
		minAgeField = new JTextField("");
		maxAgeField = new JTextField("");
		timeField = new ScorePanel();
		eventCodeBox = EventComboBox.getInstance();
		eventNameField = new JTextField("");
		numHeatsField = new JTextField("");
		genderBox = new JComboBox (Heat.GENDER_OPTIONS);
		
		/* Associate event name with event code via action listener */
		eventCodeBox.addRelationshipListener(eventNameField);
		/* Listener that triggers unit label changes */
		eventCodeBox.addUnitListener(timeField);
		
		final JPanel modiPanel = new JPanel();
		final GroupLayout groupLayout = new GroupLayout(modiPanel);
		modiPanel.setLayout(groupLayout);
		groupLayout.setAutoCreateGaps(true);
		groupLayout.setAutoCreateContainerGaps(true);	
		
		groupLayout.setHorizontalGroup(
			groupLayout.createSequentialGroup()
		   		.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
	   				.addComponent(eventNumLabel)
	   				.addComponent(eventNameLabel)
	   				.addComponent(genderLabel)
	   				.addComponent(minAgeLabel)
	   				.addComponent(maxAgeLabel)
	   				.addComponent(timeLabel)
	   				.addComponent(numHeatsLabel)
	   				)
	   			.addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
	   				.addComponent(eventCodeBox)
	   				.addComponent(eventNameField)
	   				.addComponent(genderBox)
	   				.addComponent(genderLabel)
	   				.addComponent(minAgeField)
	   				.addComponent(maxAgeField)
	   				.addComponent(timeField)
	   				.addComponent(numHeatsField)
	   				)
		);

		groupLayout.setVerticalGroup(
		   groupLayout.createSequentialGroup()
		   	   .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(eventNumLabel)
		           .addComponent(eventCodeBox))
		       .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(eventNameLabel)
		           .addComponent(eventNameField))
	           .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(genderLabel)
		           .addComponent(genderBox))
	           .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		   .addComponent(minAgeLabel)
		           .addComponent(minAgeField))
		       .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(maxAgeLabel)
		           .addComponent(maxAgeField))
	           .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
		           .addComponent(timeLabel)
		           .addComponent(timeField))
	           .addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
		           .addComponent(numHeatsLabel)
		           .addComponent(numHeatsField))
		);
		
		top.add(modiPanel, BorderLayout.CENTER);
		top.add(buttons, BorderLayout.PAGE_END);
	
		JTextArea commentBox = new JTextArea("");
		commentBox.setEditable(true);
		commentBox.setBorder(BorderFactory.createLineBorder(new Color(99, 150, 225), 1));
		commentBox.setFont(new Font("Serif", Font.PLAIN, 14));
		commentBox.setLineWrap(true);
		commentBox.setWrapStyleWord(true);
		JScrollPane sp = new JScrollPane(commentBox);
		sp.setPreferredSize(new Dimension(100, 100));
		sp.setBorder(BorderFactory.createTitledBorder("COMMENTS"));
		
		bottom.add(sp, BorderLayout.CENTER);

		this.add(top, BorderLayout.CENTER);
		this.add(bottom, BorderLayout.PAGE_END);
	}
	

	public void saveNewHeat() {
		String errMsg = "Error: Invalid Data Entered";
		ErrDialog errDialog = new ErrDialog(errMsg);

		try {
			createController.createNewHeat();
		} catch(Exception ex) {
			String msg = ex.getLocalizedMessage();
			if (msg == null) return;
			
			if (msg.contains("\"")) {
				msg = msg.substring(msg.indexOf("\""));
				msg = msg.replaceAll("\"", "");
			}
			
			else if (msg.equals(ErrDialog.GENDER_NS_ERROR)) {
				errMsg = "Error: no gender was selected.";
			}	
			else if (msg.equals(ErrDialog.AGE_WF_ERROR) || 
					msg.equals(ErrDialog.AGE_RANGE_ERROR)) {
				errMsg = "Error: age must be a number between 6 - 99.";
			}
			else if (msg.equals(ErrDialog.EVENTCD_NULL_ERROR)) {
				errMsg = "Error: no events have been created. Create an event" +
						" before attempting to register an athlete.";
				new WarningTimer(NavMenu.events);
			}	
			else if (msg.equals(ErrDialog.EVENTCD_NS_ERROR)) {
				errMsg = "Error: no event code was selected.";
			}
			
			else if (msg.contains(ErrDialog.SCORE_RANGE_ERROR)) {
				errMsg = msg;
			}
			
			
			errDialog.setDialog(errMsg);
			errDialog.setVisible(true);
			return;
		}
	}

	public static JTextField getMinAgeField() {
		return minAgeField;
	}
	public static JTextField getMaxAgeField() {
		return maxAgeField;
	}
	public static ScorePanel getTimeField() {
		return timeField;
	}
	public static JComboBox getEventCodeBox() {
		return eventCodeBox;
	}
	public static JComboBox getGenderBox() {
		return genderBox;
	}
	public static JTextField getEventNameField() {
		return eventNameField;
	}
	public static JTextField getNumHeatsField() {
		return numHeatsField;
	}
	
	public void configureButtons() {
		addButton = new JButton("Add");
		addButton.setBackground(new Color(0, 100, 0));
		addButton.setForeground(Color.WHITE);
		addButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				saveNewHeat();
			}}
		);
	}
}

