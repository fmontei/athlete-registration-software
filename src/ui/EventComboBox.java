package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.JComboBox;
import javax.swing.JTextField;

import datastore.DataStore;
import domain.Event;

public class EventComboBox extends JComboBox {
	private static final long serialVersionUID = -2342621394652901284L;
	
	private static int EVENT_BOX_COUNT = 0;
	private static final int MAX_NUM_INSTANCES = 5;
	private static Vector <EventComboBox> INSTANCES = 
		new Vector <EventComboBox> (MAX_NUM_INSTANCES);
	
	/* Hash map that stores the event according to its event code in a bucket */
//	private static HashMap <String, Event> EVENT_CODES = 
//			new HashMap <String, Event> ();
	
	private EventComboBox() {
	}
	
	private static Comparator <Event> compCode = new Comparator <Event> () {
		public int compare(Event e1, Event e2) {
			return e1.getCode().compareTo(e2.getCode());
		}
	};
	
//	public static List<Event> getEvents(){
//		
//			List <Event> list = new ArrayList <Event> ();
//			Iterator<Entry <String, Event>> it = 
//					EVENT_CODES.entrySet().iterator();
//		    while (it.hasNext()) {
//		        Map.Entry <String, Event> pair = it.next();
//		        
//		       Event temp = pair.getValue();
//		        list.add(temp);
//		        
//		    }
//		    return list;
//	}
	
	protected static EventComboBox getInstance() {
		if (EVENT_BOX_COUNT < MAX_NUM_INSTANCES) {
			EventComboBox temp = new EventComboBox();
			INSTANCES.add(temp);
			
			EVENT_BOX_COUNT++;
			/* Load and sort events into combo boxes after 
			 * they've all been instantiated */
			if (EVENT_BOX_COUNT == MAX_NUM_INSTANCES) {
				popData();
			}
			
			return temp;
		}
		
		return null;
	}
	
	private static void popData(){
		List <Event> rawData = DataStore.getInstance().getEvents();
		Collections.sort(rawData, compCode);
		
		for (Event e : rawData) 
			EventComboBox.addByCode(e.getCode(), e);
		
		rawData = null;
	}
	
	public static void setComboBoxText(JComboBox box, String entry) {
		for (int i = 0; i < box.getItemCount(); i++) {
			String boxItem = box.getItemAt(i).toString();
			/* The provided teacher names include the school name as well;
			 * thus if the teacher name contains the school name, match */
			if (entry.equals(boxItem) || boxItem.contains(entry)) {
				box.setSelectedIndex(i);
				break;
			}
			else
				box.setSelectedIndex(-1);
		}
	}
	
	public static void addByCode(final String code, final Event event) {
		/* HashMap will replace old event with new event if the codes 
		 * are identical, allowing for event codes to be updated */
		Event.addByCode(code, event);
		EventComboBox.updateInstances(code, event);
	}
	
	private static void updateInstances(final String newCode, 
			final Event newEvent) {

		/* Prevents duplicate codes from being added to event code boxes */
		final JComboBox temp = INSTANCES.elementAt(0);
		for (int i = 0; i < temp.getItemCount(); i++) {
			if (temp.getItemAt(i).toString().equals(newCode)) return;
		}
		
		/* Adds the new event code to every combo box across program */
		for (int i = 0; i < MAX_NUM_INSTANCES; i++) {
			if (INSTANCES.elementAt(i) == null) continue;
			INSTANCES.elementAt(i).addItem(newCode);
			INSTANCES.elementAt(i).setSelectedIndex(-1);
			INSTANCES.elementAt(i).repaint();
		}
	}
	
	public void addUnitListener(final ScorePanel panel) {
		this.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (getItemCount() > 0 && getSelectedIndex() > -1) {		
					String code = getSelectedItem().toString();
					Event curEvent = Event.lookUpByCode(code);
					String unit = curEvent.getScoreUnit();
					
					if (unit.equals(ScorePanel.T)) {
						panel.setEnabled(true);
						panel.setToTimeUnits();
					}
					else if (unit.equals(ScorePanel.D)) {
						panel.setEnabled(true);
						panel.setToDistanceUnits();
					}
					else if (unit.equals(ScorePanel.N)) {
						panel.clearText();
						panel.clearUnits();
						panel.setEnabled(false);
					}
				}
			}
		});
	}
	
	public void addRelationshipListener(final JTextField nameField) {
		this.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* When an event code is selected, the event name associated
				 * with that code populates the event name field automatically */
				if (isShowing() && getSelectedIndex() > -1) {
					final String key = getSelectedItem().toString();
					nameField.setText(Event.lookUpByCode(key).getName());
				}
				else {
					nameField.setText("");
				}
			}
		});
	}
}

