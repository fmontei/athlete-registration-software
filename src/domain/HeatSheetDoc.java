package domain;

public class HeatSheetDoc {
	private String heatSheets;
	private int pagesInDoc;
	public HeatSheetDoc(){
		heatSheets = "";
		pagesInDoc = 0;
	}
	public String getHeatSheets() {
		return heatSheets;
	}
	public void setHeatSheets(String heatSheets) {
		this.heatSheets = heatSheets;
	}
	public int getPagesInDoc() {
		return pagesInDoc;
	}
	public void setPagesInDoc(int pagesInDoc) {
		this.pagesInDoc = pagesInDoc;
	}
	public void append(char c) {
		heatSheets += c;
		
	}
	public void append(String string) {
		heatSheets += string;
		
	}
	
}
