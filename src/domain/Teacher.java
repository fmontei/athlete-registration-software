package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class Teacher implements Serializable{

	private static final long serialVersionUID = 6248666037907557765L;
	private String name;
	private School school;
	
	private ArrayList<Athlete> athletes = new ArrayList<Athlete>();
	private HashMap<String, Integer> map = new HashMap<String, Integer>();
	private static Vector <String> allGroupCodes = new Vector <String> ();
	
	public Teacher(String name) {
		this.name = name;
	}
	
	public Teacher() {
		
	}

	public void addAthlete(Athlete athlete) {
		athletes.add(athlete);
		map.put(athlete.getFirstName() + athlete.getLastName(), athletes.size());
	}
	
	public Athlete getAthlete(String name) {
		return athletes.get(map.get(name));
	}
	
	public ArrayList<Athlete> getAthletes() {
		return athletes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}
	
	public String toString() {
		return name + " of " + school.getName();
	}
	
	public static Vector <String> getGroupCodes() {
		return allGroupCodes;
	}
}
