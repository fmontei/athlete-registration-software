package domain;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class Heat implements Serializable {
	
	private static final long serialVersionUID = 8348886324825066731L;
	
	public static final String EVENT_CODE = "Event Code";
	public static final String EVENT_NAME  = "Event Name";
	public static final String GENDER = "Gender";
	public static final String MIN_AGE  = "Min Age";
	public static final String MAX_AGE = "Max Age";
	public static final String TIME  = "Time";
	public static final String NUM_HEATS = "Number of Heats";
	public static final int FIELDS = 7;
	public static final String GENDER_OPTIONS[] = {"B", "M", "F"};
	
	private String eventCode;
	private String eventName;
	private String gender;
	private Integer minAge;
	private Integer maxAge;
	private String time;
	private Integer numHeats;
	private Event event;
	private ArrayList <Athlete> athletes;
	
	public Heat(String eventCode, String eventName, String gender, Integer minAge, 
			Integer maxAge, String time, Integer numHeats) {
		
		this.eventCode = eventCode;
		this.eventName = eventName;
		this.gender = gender;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.time = time;
		this.numHeats = numHeats;
		this.athletes = new ArrayList <Athlete> ();
	}
	public static Comparator <Heat> compHeatByTime =  new Comparator <Heat> () {
		public int compare(Heat a1, Heat a2) {
			return a1.time.compareTo(a2.time);
		}
	};
	
	public Heat() {
		// TODO Auto-generated constructor stub
	}

	public int getSizeAthletes() {
		return athletes.size();
	}
	
	public ArrayList<Athlete> getAthletes() {
		return athletes;
	}
	
	public Athlete removeAthlete(final int index) {
		Athlete temp = athletes.get(index);
		athletes.remove(index);
		return temp;
	}
	
	public void addAthlete(final Athlete newAthlete) {
		athletes.add(newAthlete);
	}
	
	public String getEventCode() {
		return eventCode;
	}

	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getNumHeats() {
		return numHeats;
	}

	public void setNumHeats(int numHeats) {
		this.numHeats = numHeats;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public String getColumn(int col) {
		switch(col) {
			case 0:
				return eventCode;
			case 1:
				return eventName;
			case 2: 
				return gender;
			case 3:
				return Integer.toString(minAge);
			case 4: 
				return Integer.toString(maxAge);
			case 5:
				return time;
			case 6:
				return Integer.toString(numHeats);
			default:
				return "Error";
		}
	}
	
	public String toString() {
		String r_string = eventCode + " ";
		r_string += eventName + " ";
		r_string += gender + " ";
		r_string += minAge + " ";
		r_string += maxAge + " ";
		r_string += time + " ";
		r_string += numHeats + " ";
		return r_string;
	}

	public int generateString(HeatSheetDoc doc) throws ParseException {
		//Max number of athletes per page: 57
		int pgMax = 57;
		int i = 0;
		boolean running = false;
		int rank = 0;
		int div = 1;
		//Don't generate a heat with no athletes (or number of heats is zero)
		if(athletes.size() == 0 || numHeats == 0){
			return 0;
		}
		int htMax = athletes.size()/numHeats;

		do{
			//Put page header on doc
			doc.append(generatePageHead(doc));
			//Cycle through athletes, append each one to the doc til  the page is full or
			//it's time for the next heat
			for(;i<athletes.size(); i++){
				rank++;
				doc.append(athletes.get(i).generateString(rank, div)+"\r\n");

				//Filled this heat. Now do the next
				if(i > 0 && (i+1)%htMax == 0 && i != athletes.size()-1){
					div += 1;
					rank = 0;
					running = true;
					doc.append((char)12);
					i++;
					break;
				}
				//Hit the max number for this page, do another page
				else if(i > 0 && (i+1)%pgMax == 0 && i != athletes.size()-1){
					running = true;
					doc.append((char)12);
					i++;
					break;
				}
				running = false;
			}
		}while(running);
		return 1;
	}

	private String generatePageHead(HeatSheetDoc doc) throws ParseException {
		//Generate header for a page
		
		doc.setPagesInDoc(doc.getPagesInDoc()+1);
		String prntTime = time.replace('.', ':');
		String strEv = eventName;
		String gen = "Both";
		DateFormat f1 = new SimpleDateFormat("hh:mm:ss");
		DateFormat f2 = new SimpleDateFormat("h:mm");
		Date d = f1.parse(prntTime);
		prntTime = f2.format(d);
		
		if(gender.charAt(0) == 'M'){
			gen = "Men";
		}
		if(gender.charAt(0) == 'F'){
			gen = "Women";
		}
		if(eventName.length() > 17){
			strEv = eventName.substring(0,16);
		}
		for(int i=strEv.length();i<17;i++){
			strEv += " ";
		}
		String str =strEv+
				"\t\t  Group"+
				"\t"+minAge+" to "+maxAge+
				"\t\tTime "+prntTime+
				"\t\t         Page "+doc.getPagesInDoc()+"\r\n";
		str +="For "+gen+"\r\n";
		str+="----------------------------------------------------------"+
								"---------------------------------------\r\n";
		str+="Name\t\t\t\t  Age  Sx Rank\t   Time Div.  Group\t       Supervisor\r\n";
		str+="----------------------------------------------------------"+
		                        "---------------------------------------\r\n";
		return str;
	}

	public void clearHeat() {
		athletes.clear();
		
	}
}
