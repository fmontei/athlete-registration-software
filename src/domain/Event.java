package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import datastore.DataStore;

public class Event implements Serializable {
	
	private static final long serialVersionUID = 703820070218115561L;
	
	public static final String EVENT_CODE = "Event Code";
	public static final String EVENT_NAME = "Event Name";
	public static final String SCORE_UNIT = "Score Unit";
	public static final String SCORE_MIN  = "Min Score";
	public static final String SCORE_MAX  = "Max Score";
	public static final String SORT_SEQ  = "Sort Sequenece";
	public static final int FIELDS = 6;
	
	private static DataStore dat;

	private String code;
	private String name;
	private String scoreUnit;
	private Float scoreMin;
	private Float scoreMax;
	private Integer sortSeq;
	private boolean editable;
	private ArrayList <Athlete> athletes;
	private ArrayList <Heat> heats;
	
	private static HashMap <String, Event> EVENT_CODES = 
			new HashMap <String, Event> ();
	
	private static HashMap <String, Vector <Athlete>> allAthletes =
			new HashMap <String, Vector <Athlete>> ();

	public Event(String eventCode, String eventName, String scoreUnit,
			float scoreMin, float scoreMax, int sortSeq) {
		this.code = eventCode;
		this.name = eventName;
		this.scoreUnit = scoreUnit;
		this.scoreMin = scoreMin;
		this.scoreMax = scoreMax;
		this.sortSeq = sortSeq;
		this.athletes = new ArrayList <Athlete> ();
		this.heats = new ArrayList <Heat> ();
	}
	
	public Event() {
	}
	
	public static Event lookUpByCode(final String code) {
		return Event.EVENT_CODES.get(code);
	}
	
	public static void popEvents(){
		EVENT_CODES.clear();
		dat = DataStore.getInstance();
		ArrayList<Event> temp = dat.getEvents();
		for(Event ev: temp){
			EVENT_CODES.put(ev.code, ev);
		}
	}
	
	public static void popAthletes(){
		
		allAthletes.clear();
		for(Event ev: getEvents()){
			ev.athletes.clear();
		}
		ArrayList<Athlete> temp = dat.getAthletes();
		for(Athlete ath: temp){
			Event ev = lookUpByCode(ath.getEventCode());
			
			ev.addAthlete(ath);
		}
		
	}

	public static void linkHeats(){
		dat = DataStore.getInstance();
		ArrayList<Event> evs = (ArrayList<Event>) Event.getEvents();
		for(Event ev: evs){
			ev.heats.clear();
		}
		for(Heat h:dat.getHeats()){
				h.clearHeat();
				Event ev = lookUpByCode(h.getEventCode());
				ev.addHeat(h);
			}
		
	}
	
	public static List<Event> getEvents(){
		
		List <Event> list = new ArrayList <Event> ();
		Iterator<Entry <String, Event>> it = 
				EVENT_CODES.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry <String, Event> pair = it.next();
	        
	       Event temp = pair.getValue();
	        list.add(temp);
	        
	    }
	    return list;
	}
	public static List<Vector<Athlete>> getAllAthletes(){
		
		List <Vector<Athlete>> list = new ArrayList <Vector<Athlete>> ();
		Iterator<Entry <String, Vector<Athlete>>> it = 
				allAthletes.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry <String, Vector<Athlete>> pair = it.next();
	        
	       Vector<Athlete> temp = pair.getValue();
	        list.add(temp);
	        
	    }
	    return list;
	}
	public static void addByCode(final String code, final Event event) {
		/* HashMap will replace old event with new event if the codes 
		 * are identical, allowing for event codes to be updated */
		EVENT_CODES.put(code, event);
	}
	public static Comparator <Event> compSortSeq =  new Comparator <Event> () {
		public int compare(Event a1, Event a2) {
			return a1.sortSeq - a2.sortSeq;
		}
	};
	
	public static ArrayList<Event> getEventsSortedBySortSeq(){
		ArrayList<Event> evs = (ArrayList<Event>) getEvents();
		Collections.sort(evs, compSortSeq);
		return evs;
	}
		

	public ArrayList<Athlete> getAthletes() {
		return athletes;
	}
	
	public ArrayList<Heat> getHeats() {
		return heats;
	}

	public void removeAthlete(final Athlete athlete) {
		Vector <Athlete> temp = allAthletes.get(generateKey(athlete));
		if (temp.contains(athlete)) {
			temp.remove(temp.indexOf(athlete));
		}
		
		athletes.remove(athlete);
	}
	
	public int addAthlete(final Athlete newAthlete) {
		int success = addAthleteToList(newAthlete);
		if (success == 1)
			athletes.add(newAthlete);
		
		return success;
	}
	
	public void removeHeat(final Heat heat) {
		heats.remove(heat);
	}
	
	public void addHeat(final Heat newHeat) {
		heats.add(newHeat);
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String eventCode) {
		this.code = eventCode;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String eventName) {
		this.name = eventName;
	}
	
	public String getScoreUnit() {
		return scoreUnit.toString();
	}
	
	public void setScoreUnit(String x) {
		this.scoreUnit = x;
	}
	
	public float getScoreMin() {
		return scoreMin;
	}
	
	public void setScoreMin(float x) {
		this.scoreMin = x;
	}
	
	public float getScoreMax() {
		return scoreMax;
	}
	
	public void setScoreMax(float x) {
		this.scoreMax = x;
	}
	
	public String getSortSeq() {
		return sortSeq.toString();
	}
	
	public void setSortSeq(String x) {
		try {
			sortSeq = Integer.parseInt(x);
		} catch(NumberFormatException e) {
			sortSeq = 0;
		}
	}
	
	public String getColumn(int col) {
		switch(col) {
			case 0:
				return code;
			case 1: 
				return name;
			case 2:
				return scoreUnit;
			case 3: 
				return scoreMin.toString();
			case 4:
				return scoreMax.toString();
			case 5:
				return sortSeq.toString();
			default:
				return "Error";
		}
	}
	
	public boolean isEditable() {
		return editable;
	}
	
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	public static boolean verifyEventCode(final String evTxt) {	
		/* Verifies whether event code conforms to right format */
		if (evTxt.length() != 4)
			return false;
		else if (!evTxt.toLowerCase().contains("e"))
			return false;
		/* Parses remaining characters to check if they are numbers */
		try {
			Integer.parseInt(evTxt.substring(1, evTxt.length()));
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	public static int addAthleteToList(final Athlete newAthlete) {
		String key = generateKey(newAthlete);
		Vector<Athlete> vec = allAthletes.get(key);
		
		/* Athlete has not yet been registered for any event */
		if (vec == null || vec.size() == 0) {
			vec = new Vector <Athlete> ();
			vec.add(newAthlete);
			allAthletes.put(key, vec);
			return 1;
		}
		/* Athlete is eligible to be registered */
		else if (vec.size() == 1) {
			/* Prevents athlete from being registered in the same event twice */
			String curCode = vec.get(0).getEventCode();
			String newCode = newAthlete.getEventCode();
			if (curCode.equals(newCode)) {
				return -1;
			}
			
			else {
				vec.add(newAthlete);
				allAthletes.put(key, vec);
				return 1;
			}
		}
		/* Athlete is registered for too many events */
		else if (vec.size() > 1) {
			return -2; 
		}
		
		return 1;
	}
	
	public static ArrayList <Athlete> getListOfAthletes(final int compareCode) {
		ArrayList <Athlete> list = new ArrayList <Athlete> ();
		Iterator<Entry <String, Vector <Athlete>>> it = 
			allAthletes.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry <String, Vector <Athlete>> pair = it.next();
	        
	        Vector <Athlete> temp = pair.getValue();
	        for (Athlete a : temp)
	        	list.add(a);
	    }
	    
	    /* Sorts athlete list according to different parameters */
	    switch(compareCode) {
		    case Athlete.COMPARE_BY_NAME:
		    	Collections.sort(list, Athlete.compFirstName);
		    	break;
		    case Athlete.COMPARE_BY_AGE:
		    	Collections.sort(list, Athlete.compAge);
		    	break;
		    case Athlete.COMPARE_BY_SCORE:
		    	Collections.sort(list, Athlete.compScore);
		    	break;
		    case Athlete.COMPARE_BY_EVENT_CODE:
		    	Collections.sort(list, Athlete.compEventCode);
		    	break;
	    }
	  
 	    return list;
	}
	
	private static String generateKey(final Athlete a) {
		/* Returns a unique key to avoid collisions with different
		 * athletes who go by the same name and have similar characteristics */
		return a.getFirstName() + a.getLastName() + a.getAge() 
				+ a.getSchool();
	}
	public void sortAthletesInEvent(final int compareCode) {
	    /* Sorts athlete list according to different parameters */
	    switch(compareCode) {
		    case Athlete.COMPARE_BY_NAME:
		    	Collections.sort(athletes, Athlete.compFirstName);
		    	break;
		    case Athlete.COMPARE_BY_AGE:
		    	Collections.sort(athletes, Athlete.compAge);
		    	break;
		    case Athlete.COMPARE_BY_SCORE:
		    	Collections.sort(athletes, Athlete.compScore);
		    	break;
		    case Athlete.COMPARE_BY_EVENT_CODE:
		    	Collections.sort(athletes, Athlete.compEventCode);
		    	break;
		    case Athlete.COMPARE_BY_FULL_NAME:
		    	Collections.sort(athletes, Athlete.compFullName);
		    case Athlete.COMPARE_BY_SCORE_OPP:
		    	Collections.sort(athletes, Athlete.compScoreOp);
	    }
	  
	}
	public int generateSheets(HeatSheetDoc doc) throws Exception {
		//sort the heats by time of day
		Collections.sort(heats, Heat.compHeatByTime);
		//put athletes into heats
		int ret = populateHeats();
		
		int wasGenerated = 0;
		//Generate the sheets for each heat, add it to the HeatSheetDoc
		for(int i=0; i<heats.size();i++){
			wasGenerated = heats.get(i).generateString(doc);
			if((i < heats.size() -1) && wasGenerated != 0){
				doc.append((char)12);
			}
		}
		return ret;
	}  

	
	private int populateHeats() throws Exception{
		//This function splits athletes in an event into heats
		if(heats.isEmpty()){
			throw new Exception();
		}
		
		//sort the athletes properly according to score/name
		if(!(scoreUnit.charAt(0)=='N')){
			if(scoreUnit.charAt(0) == 'T')
				sortAthletesInEvent(Athlete.COMPARE_BY_SCORE);
			else
				sortAthletesInEvent(Athlete.COMPARE_BY_SCORE_OPP);
			sortAppropriately();
		}
		else{
			sortAthletesInEvent(Athlete.COMPARE_BY_FULL_NAME);
		}
		
		ArrayList<Athlete> tempHolder = new ArrayList<Athlete>();
		int numHeatsTotal = 0;
		//Figure out how many heats are in this Event total
		for(Heat ht: heats){
			numHeatsTotal += ht.getNumHeats();
		}
		
		//Figure out approximately how many athletes per heat that means
		int athsPerHeat = athletes.size()/numHeatsTotal;
		
		int numForHeat = 0;
		int i = 0;
		int b = 0;
		
		//Cycle through heats and add athletes into the first heat they can enter, according to age and gender
		for(Heat ht: heats){
			ht.clearHeat();
			numForHeat = athsPerHeat * ht.getNumHeats();
			for(; i<athletes.size(); i++){
				if((ht.getGender().charAt(0) == 'B' ||
						ht.getGender().charAt(0) == athletes.get(i).getGender().charAt(0))
					&& (athletes.get(i).getAge() <= ht.getMaxAge() && 
							athletes.get(i).getAge() >= ht.getMinAge())
				){
				
					ht.addAthlete(athletes.get(i));b++;
					tempHolder.add(athletes.remove(i));
					i--;
				}
				if(b == numForHeat){
					i=0;
					b=0;
					break;
				}
			}	
		}
		
		//If there are athletes that were skipped over, go and try to add them again, this time from last heat to first
		if(athletes.size() > 0){
			for(int x=0; x<athletes.size();x++){
				for(int y=heats.size()-1; y>=0;y--){
					Heat ht = heats.get(y);
					if((ht.getGender().charAt(0) == 'B' ||
							ht.getGender().charAt(0) == (athletes.get(x).getGender().charAt(0)))
							&& (athletes.get(x).getAge() <= ht.getMaxAge() && 
									athletes.get(x).getAge() >= ht.getMinAge())
						){
					
						ht.addAthlete(athletes.get(x));
						tempHolder.add(athletes.remove(x));
						x--;
						break;
					}
				}
			}
		}
		//Return the number of un-added athletes, and restore the athletes in the Event
		int sz = athletes.size();
		athletes.addAll(tempHolder);
		tempHolder.clear();
		return sz;
		
	}
	public int LowestAthWithSameScore(ArrayList<Athlete> ath, int start, float sr){
		//Get the athlete with the lowest rank with the same score as the athlete in rank 'start'
		int ind = start;
		for(int i=start; i<ath.size();i++){
			if(ath.get(i).getScore() == sr){
				ind = i;
			}
		}
		return ind;
	}
	public void sortAppropriately(){
		//Once athletes are sorted by score, go and sort same-score athletes by name
		ArrayList<Athlete> lst;
		for(int i=0; i<athletes.size(); i++){
			lst = new ArrayList<Athlete>();
			int ind = LowestAthWithSameScore(athletes, i, athletes.get(i).getScore());
			
			for(int b = i; b<= ind; b++){
				lst.add(athletes.get(b));
			}
			Athlete.sortAthletesInList(lst, Athlete.COMPARE_BY_FULL_NAME);
			for(int b = i; b <= ind; b++){
				athletes.set(b, lst.get(b-i));
			}
		}
	}
	
	public String toString() {
		return name;
	}
}

