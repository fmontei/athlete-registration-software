package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.Map.Entry;

public class School implements Serializable{

	private static final long serialVersionUID = -2414802449744128443L;
	private String name;
	
	private ArrayList <Teacher> teachers = new ArrayList<Teacher>();
	private static HashMap <String, Vector <Teacher>> allSchools = 
			new HashMap <String, Vector <Teacher>> ();
	
	public School() {
		
	}
	
	public void addTeacher(Teacher teacher) {
		if(teacher != null && !teachers.contains(teacher)) teachers.add(teacher);
	}
	
	public School(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}

	public static HashMap <String, Vector <Teacher>> getSchoolList() {
		return allSchools;
	}
	
	public static ArrayList <String> getListOfSchoolNames() {
		ArrayList <String> list = new ArrayList <String> ();
		Iterator <Entry <String, Vector <Teacher>>> it = 
			allSchools.entrySet().iterator();
	    while (it.hasNext()) {
	        Entry <String, Vector <Teacher>> pair = it.next();
	        
	        String name = pair.getKey();
	        list.add(name);
	    }
	    
	    Collections.sort(list);
	    return list;
	}
}
