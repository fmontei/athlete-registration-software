package domain;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class Athlete implements Serializable{
	
	private static final long serialVersionUID = 5750676438026582843L;
	
	public static final String FIRST_NAME = "First Name";
	public static final String LAST_NAME  = "Last Name";
	public static final String AGE        = "Age";
	public static final String GENDER     = "Gender";
	public static final String SCORE      = "Score";
	public static final String SCHOOL     = "School";
	public static final int FIELDS = 10;
	
	public static final int MIN_AGE = 6;
	public static final int MAX_AGE = 99;
	
	/* Test values */
	public static final String[] GRP_CODES = 
		{"G001", "G002", "G002", "G003", "G004", "G005"};
	public static final String[] GENDERS = {"M", "F"};
	
	public final static int COMPARE_BY_NAME        = 0;
	public final static int COMPARE_BY_AGE         = 1;
	public final static int COMPARE_BY_SCORE       = 2;
	public final static int COMPARE_BY_EVENT_CODE  = 3;
	public static final int COMPARE_BY_FULL_NAME   = 4;
	public static final int COMPARE_BY_SCORE_OPP   = 5;
	private static int COMPARATOR_SIGN = 1;
	
	public static void invertComparatorSign() {
		COMPARATOR_SIGN *= -1;
	}
	public static void resetComparatorSign() {
		COMPARATOR_SIGN = 1;
	}
	
	/* Comparators that sort athletes according to comperent parameters */
	public static Comparator <Athlete> compFirstName =  new Comparator <Athlete>() {
		public int compare(Athlete a1, Athlete a2) {
			int comp = a1.getFirstName().compareTo(a2.getFirstName());
    		
			/* The comparator prevents redundant athletes from being added;
			 * if the names are the same, equality is assumed.
			 * Solution: if the names are the same and the athletes
			 * are NOT the same -- don't have same unique key -- return 1
			 */
			if (comp == 0 && 
					!a1.getEventCode().equals(a2.getEventCode()))
				return 1;
    		return comp * COMPARATOR_SIGN;
		}
    };
    public static Comparator <Athlete> compAge =  new Comparator <Athlete> () {
		public int compare(Athlete a1, Athlete a2) {
			return (a1.getAge() - a2.getAge()) * COMPARATOR_SIGN;
		}
    };
    public static Comparator <Athlete> compSchool =  new Comparator <Athlete> () {
		public int compare(Athlete a1, Athlete a2) {
			return a1.getSchool().compareTo(a2.getSchool()) * COMPARATOR_SIGN;
		}
    };
    public static Comparator <Athlete> compFullName = new Comparator <Athlete>(){
    	public int compare(Athlete a1, Athlete a2){
    		String f1 = a1.last + a1.first;
    		String f2 = a2.last + a2.first;
    		int comp = (f1.compareTo(f2));
    		
    		if (comp == 0 && 
				!a1.getEventCode().equals(a2.getEventCode()))
				return 1;
    		return comp * COMPARATOR_SIGN;
    	}
    };
    public static Comparator <Athlete> compEventCode =  new Comparator <Athlete> () {
		public int compare(Athlete a1, Athlete a2) {
			int cmp = a1.getEventCode().compareTo(a2.getEventCode());
			return cmp * COMPARATOR_SIGN;
		}
    };
    public static Comparator <Athlete> compScore =  new Comparator <Athlete> () {
		public int compare(Athlete a1, Athlete a2) {
			float cmp = (a1.getScore() - a2.getScore()) * 1000;
			return (int) cmp * COMPARATOR_SIGN;
		}
    };
    public static Comparator <Athlete> compScoreOp =  new Comparator <Athlete> () {
		public int compare(Athlete a1, Athlete a2) {
			float cmp = (a2.getScore() - a1.getScore()) * 1000;
			return (int) cmp * COMPARATOR_SIGN; 
		}
    };
    
    public static void sortAthletesInList(List<Athlete> athletes, final int compareCode) {	    
	    /* Sorts athlete list according to comperent parameters */
	    switch(compareCode) {
		    case Athlete.COMPARE_BY_NAME:
		    	Collections.sort(athletes, Athlete.compFirstName);
		    	break;
		    case Athlete.COMPARE_BY_AGE:
		    	Collections.sort(athletes, Athlete.compAge);
		    	break;
		    case Athlete.COMPARE_BY_SCORE:
		    	Collections.sort(athletes, Athlete.compScore);
		    	break;
		    case Athlete.COMPARE_BY_EVENT_CODE:
		    	Collections.sort(athletes, Athlete.compEventCode);
		    	break;
		    case Athlete.COMPARE_BY_FULL_NAME:
		    	Collections.sort(athletes, Athlete.compFullName);
		    	break;
	    }
}
	private String  first;
	private String  last;
	private String  gender;
	private Integer age;
	private Float   score;
	private String  school;
	private String  eventCode;
	private String  eventName;
	private String  grpCode;
	private String  grpLeader;
	private boolean editable;
	private Event event1;
	private Event event2;
	//private List <Event> events;
	
	public Athlete() {
		first = last = gender = school = eventCode = eventName = grpCode = grpLeader = "";
		age = 0; 
		score = 0.0f;
	}
	
	public Athlete(String first, String last, String gender,
			Integer age, String school, Float score, String eventCode, 
			String eventName, String grpCode, String grpLeader) {
		
		this.first = first;
		this.last = last;
		this.gender = gender;
		this.age = age;
		this.score = score;
		this.school = school;
		this.eventCode = eventCode;
		this.eventName = eventName;
		this.grpCode = grpCode;
		this.grpLeader = grpLeader;
		this.editable = false;
	}
	
	public String generateUniqueKey() {
		/* Returns a unique key to avoid collisions with different
		 * athletes who go by the same name and have similar characteristics */
		return this.getFirstName() + this.getLastName() + this.getAge() 
				+ this.getSchool();
	}
	
	public Event getEvent1() {
		return event1;
	}
	
	public Event getEvent2() {
		return event2;
	}
	
	public void setEvent1(Event event) {
		event1 = event;
	}
	
	public void setEvent2(Event event) {
		event2 = event;
	}
	
	public String getFirstName() {
		return first;
	}
	
	public String getLastName() {
		return last;
	}
	
	public Integer getAge() {
		return age;
	}
	
	public String getSchool() {
		return school;
	}
	
	public Float getScore() {
		return score; 
	}
	
	public String getEventName() {
		return eventName;
	}
	
	public String getEventCode() {
		return eventCode;
	}
	
	public boolean getEditable() {
		return editable;
	}
	
	public void setFirstName(String f) {
		first = f;
	}
	
	public void setLastName(String l) {
		last = l;
	}
	
	public void setGender(String g) {	
		gender = g;
	}
	
	public void setAge(String a) {
		try {
			age = Integer.parseInt(a);
		} catch(NumberFormatException e) {
			age = 0;
		}
	}
	
	public void setSchool(String s) {
		school = s;
	}
	
	public void setScore(String s) {
		try {
			score = Float.parseFloat(s);
		} catch(NumberFormatException e) {
			score = 0.0f;
		}
	}
	
	public void setEventCode(String ec) {
		eventCode = ec;
	}
	
	public void setEventName(String en) {
		eventName = en;
	}
	
	public void setGrpCode(String gc) {
		grpCode = gc;
	}
	
	public void setGrpLeader(String gl) {
		grpLeader = gl;
	}
	
	public void setEditable(boolean e) {
		editable = e;
	}
	
	public boolean hasTwoEvents() {
		if(event2 != null) return true;
		return false;
	}
	
	public String getColumn(int col) {
		switch(col) {
			case 0:
				return first;
			case 1: 
				return last;
			case 2:
				return gender;
			case 3: 
				return age.toString();
			case 4:
				return school;
			case 5:
				return score.toString();
			case 6:
				return eventCode;
			case 7:
				return eventName;
			case 8:
				return grpCode;
			case 9:
				return grpLeader;
			default:
				return "Error";
		}
	}
	
	public String generateString(int rank, int div) {
		// Generate appropriate string for an Athlete to be added to a heat
		String scoreUnit = Event.lookUpByCode(eventCode).getScoreUnit();
		String ret = "";
		String fullName = last + ", "+first;
		if(fullName.length() > 18){
			fullName = fullName.substring(0,17);
		}
		for(int i=fullName.length(); i<18;i++){
			fullName += " ";
		}
		ret += fullName;
		ret += "\t\t";
		
		String strAge = age.toString();
		for(int i=strAge.length();i<5; i++){
			strAge = " "+strAge;
		}
		ret += strAge;
		ret += "  ";
		
		ret += gender.toUpperCase().toCharArray()[0];
		
		String strRank = new Integer(rank).toString();
		for(int i=strRank.length(); i<5; i++){
			strRank = " "+strRank;
		}
		ret += strRank;
		
		String strScore = "";
		if(!(scoreUnit.charAt(0) == 'N')){
			if(score > 0.0){
				strScore = new Float(score).toString();
				if(scoreUnit.charAt(0) == 'T'){
					strScore = strScore.replace('.', ':');
				}
				else {
					strScore = strScore.replace(".", "\" ");
					strScore += "'";
				}
			}
			
		}
		for(int i=strScore.length(); i<11; i++){
			strScore = " "+strScore;
		}
		ret += strScore;
		String strDiv = new Integer(div).toString();
		for(int i=strDiv.length();i<4; i++){
			strDiv = " "+strDiv;
		}
		ret += strDiv;
		ret += "  ";
		ret += grpCode;
		ret += "\t";
		if(school.length() > 16){
			ret += school.substring(0, 15);
		}
		else {
			ret += school;
			for(int i=school.length();i<16;i++){
				ret += " ";
			}
		}
		ret+=" ";
		if(grpLeader.length() > 12){
			ret += grpLeader.substring(0, 11);
		}
		else ret += grpLeader;
		return ret;
	}

	public String getGrpCode() {
		return grpCode;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String toString() {
		return first + " " + last;
	}
}
