package controller;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

import domain.Athlete;
import domain.Event;
import domain.School;
import domain.Teacher;
import ui.AthleteCreatePanel;
import ui.AthleteMaintainPanel;
import ui.AthleteTableModel;
import ui.ErrDialog;
import ui.EventComboBox;
import ui.ScorePanel;

public class AthleteCreateController {

	private AthleteCreatePanel panel;
	private JTable table;
	private AthleteTableModel model;
	private JTextField firstField, lastField, ageField, eventNameField;
	private JComboBox eventCodeBox, genderBox, grpBox, schoolBox, teacherBox;
	private ScorePanel scoreField;
	
	public void createNewAthlete(final AthleteCreatePanel panel) throws Exception {
		initializeFields(panel);
		
		int age = 0;
		float score = -1.0f;
		
		/* Checks whether athlete's full name has been input */
		if (firstField.getText().isEmpty() || lastField.getText().isEmpty())
			throw new Exception(ErrDialog.NAME_DNE_ERROR);
		
		/* Checks if gender was selected */
		else if (genderBox.getSelectedIndex() < 0)
			throw new Exception(ErrDialog.GENDER_NS_ERROR);	
		
		/* Checks if age is between 6 - 99 */
		try {
			age = Integer.parseInt(ageField.getText());
		} catch(Exception e) { throw new Exception(ErrDialog.AGE_WF_ERROR); }
		if (age < Athlete.MIN_AGE || age > Athlete.MAX_AGE)
			throw new Exception(ErrDialog.AGE_RANGE_ERROR);
		
		/* Checks whether any events have been created */
		else if (eventCodeBox.getItemCount() == 0)
			throw new Exception(ErrDialog.EVENTCD_NULL_ERROR);
		
		/* Checks if event code has been selected */
		else if (eventCodeBox.getSelectedIndex() == -1)
			throw new Exception(ErrDialog.EVENTCD_NS_ERROR);
		
		else if (teacherBox.getSelectedIndex() == -1) 
			throw new Exception(ErrDialog.TEACHER_NS_ERROR);
		
		else if (schoolBox.getSelectedIndex() == -1) 
			throw new Exception(ErrDialog.SCHOOL_NS_ERROR);
	
		/* Validates score */ 
		if (!(scoreField.getText().equals("")) && eventCodeBox.getItemCount() != 0) {
			final String code = eventCodeBox.getSelectedItem().toString();
			float minScore = Event.lookUpByCode(code).getScoreMin();
			float maxScore = Event.lookUpByCode(code).getScoreMax();
			try {
				score = Float.parseFloat(scoreField.getText());
			} catch(Exception e) { throw new Exception(); }
			
			if (score < minScore || score > maxScore) {
				throw new Exception(ErrDialog.SCORE_RANGE_ERROR + 
					" Range must be between " + minScore + " - " + maxScore + ".");
			}
		}
		
		String first = firstField.getText();
		String last  = lastField.getText();
		String gender = genderBox.getSelectedItem().toString();
		String school = schoolBox.getSelectedItem().toString();
		String code = eventCodeBox.getSelectedItem().toString();
		String eventName = Event.lookUpByCode(code).getName();
		String grpNum = grpBox.getSelectedItem().toString();
		String grpName = teacherBox.getSelectedItem().toString();
		
		Athlete newAthlete = new Athlete(first, last, gender, age, 
				school, score, code, eventName, grpNum, grpName);
		
		int success = model.checkForRedundancies(newAthlete);
		if (success == -1) throw new Exception(ErrDialog.ATH_REG_ERROR1);
		if (success == -2) throw new Exception(ErrDialog.ATH_REG_ERROR2);

		/* Updates the athlete list for the event the athlete is registered for */
		Event temp = Event.lookUpByCode(code);
		temp.addAthlete(newAthlete);
		
		final int newRow = model.addAthlete(newAthlete);
		/* Save all data to prevent against data loss */
		model.saveData();
		
		final int row = table.getRowCount();
		model.fireTableRowsInserted(row, row);
		clearAllTextFields();
		
		/* If athlete's last name changes, update the currently selected row
		 * post-sort */
		table.scrollRectToVisible(new Rectangle(table.getCellRect(newRow, 0, true)));
		table.getSelectionModel().setSelectionInterval(newRow, newRow);
	}
	
	public void populateTeacherBox(
			final JComboBox schoolBox, 
			final JComboBox teacherBox) {
		if (schoolBox != null 
				&& teacherBox != null 
				&& schoolBox.getItemCount() > 0 
				&& schoolBox.getSelectedIndex() > -1) {
			String selection = schoolBox.getSelectedItem().toString();
			HashMap <String, Vector <Teacher>> schoolList = School.getSchoolList();
			
			Vector <Teacher> teacherList = schoolList.get(selection);
			if (teacherList != null) {
				teacherBox.setModel(new DefaultComboBoxModel(teacherList));
				teacherBox.setSelectedIndex(-1);
			}
		}
	}
	
	private void initializeFields(final AthleteCreatePanel panel) {
		/* Reference variables must point to the most up-to-date objects or 
		 * else the references will point to junk that existed upon this
		 * controller's instantiation */
		table = panel.getTable();
		model = panel.getModel();
		
		firstField = panel.getFirstNameField();
		lastField = panel.getLastNameField();
		ageField = panel.getAgeField();
		schoolBox = panel.getSchoolDropDown();
		scoreField = panel.getScoreField();
		eventNameField = panel.getEventNameField();

		genderBox = panel.getGenderDropDown();
		eventCodeBox = panel.getCodeBox();
		grpBox = panel.getGrpDropDown();
		schoolBox = panel.getSchoolDropDown();
		teacherBox = panel.getTeacherDropDown();
	}
	
	private void clearAllTextFields() {
		firstField.setText("");
		lastField.setText("");
		genderBox.setSelectedIndex(-1);
		ageField.setText(""); 
		scoreField.setText("");
		schoolBox.setSelectedIndex(-1);
		teacherBox.setSelectedIndex(-1);
		eventCodeBox.setSelectedIndex(-1);
		eventNameField.setText("");
		grpBox.setSelectedIndex(-1);
	}
}
