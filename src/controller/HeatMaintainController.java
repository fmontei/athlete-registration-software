package controller;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import domain.Athlete;
import domain.Event;
import domain.Heat;
import ui.ErrDialog;
import ui.EventComboBox;
import ui.HeatMaintainPanel;
import ui.HeatTableModel;
import ui.ScorePanel;

public class HeatMaintainController {
	private static final String[] columnNames = {"Event Code", "Event Name", 
		"Gender", "Min Age", "Max Age", "Time", "# Heats"};
	private HeatTableModel model = new HeatTableModel(columnNames);
	
	private JTable table = new JTable(model);
	private JPanel gridPanel, lowerPanel;
	
	private JTextField eventNameField, numHeatsField, minAgeField,
		maxAgeField;
	private ScorePanel timeField;
	private JComboBox eventCodeBox, genderBox;
	private JButton save, delete;
	private Integer minAge, maxAge; 
	
	public void initializeFields(){
		table = HeatMaintainPanel.getTable();
		model = HeatMaintainPanel.getModel();
		minAgeField = HeatMaintainPanel.getMinAgeField();
		maxAgeField = HeatMaintainPanel.getMaxAgeField();
		timeField = HeatMaintainPanel.getTimeField();
		eventNameField = HeatMaintainPanel.getEventNameField();
		eventCodeBox = HeatMaintainPanel.getEventCodeBox();
		genderBox = HeatMaintainPanel.getGenderBox();
		numHeatsField = HeatMaintainPanel.getNumHeatsField();
	}
	
	public void updateHeatParams() throws Exception{
		initializeFields();
		
		if(table.getSelectedRow() < 0){
			return;
		}
		
		try{
		 minAge = Integer.parseInt(minAgeField.getText());
		} catch(Exception e){
			e.printStackTrace();
			throw new Exception(ErrDialog.AGE_WF_ERROR);
		}
		
		try{	
		maxAge = Integer.parseInt(maxAgeField.getText());
		} catch(Exception e){
			e.printStackTrace();
			throw new Exception(ErrDialog.AGE_WF_ERROR);
		}
		String code = null;
		
		if(eventCodeBox.getSelectedIndex() > -1){
			 code = eventCodeBox.getSelectedItem().toString();
		}
		
		else if(!Event.verifyEventCode(code)){
			throw new Exception(code);
		}
		
		if(minAge < Athlete.MIN_AGE || maxAge > Athlete.MAX_AGE){
			throw new Exception(ErrDialog.AGE_RANGE_ERROR);
		}
		
		if (table.getSelectedRow() > -1) {
			model.setValueAt(eventNameField.getText().toString(), 
					table.getSelectedRow(), 0);
			model.setValueAt(eventCodeBox.getSelectedItem().toString(), 
					table.getSelectedRow(), 1);
			model.setValueAt(minAgeField.getText().toString(), 
					table.getSelectedRow(), 2);
			model.setValueAt(maxAgeField.getText().toString(),
					table.getSelectedRow(), 3);
			model.setValueAt(genderBox.getSelectedItem().toString(), 
					table.getSelectedRow(), 4);
			model.setValueAt(timeField.getText().toString(),
					table.getSelectedRow(), 5);

			model.setValueAt(numHeatsField.getText().toString(), 
					table.getSelectedRow(), 6);
			
			model.fireTableRowsUpdated(table.getSelectedRow(), 
					table.getSelectedRow());
			
			/* If the event code selected on the table != event code selected from
			 * drop-down, then move the athlete to the new event they're in */
			final Heat curHeat = model.getHeat(table.getSelectedRow());
			final String oldCode = curHeat.getEventCode();
			final String newCode = eventCodeBox.getSelectedItem().toString();
			
			if (!newCode.equals(oldCode)) {
				/* Removes selected athlete from the event they used to be in */
				final Event oldEvent = Event.lookUpByCode(oldCode);
				oldEvent.removeHeat(curHeat);
				/* Adds selected athlete to the new event */
				final Event newEvent = Event.lookUpByCode(newCode);
				newEvent.addHeat(curHeat);
			}
		}
	}
	
	public void deleteSelectedHeats() throws Exception {
		final int[] rows = HeatMaintainPanel.getTable().getSelectedRows();
		final int lastRow = rows.length - 1;
		if (rows.length > 0) {
			for (int i = lastRow; i >= 0; i--) {
				model.deleteHeat(rows[i]);
				/* Remove every athlete from the event they were registered for */
				final Heat curHeat = model.getHeat(rows[i]);
				final String tempCode = model.getValueAt(rows[i], 0);
				final Event tempEvent = Event.lookUpByCode(tempCode);
				tempEvent.removeHeat(curHeat);
				/* Removes athlete from table model */
				model.deleteHeat(rows[i]);
			}
			
			model.fireTableRowsDeleted(rows[0], rows[lastRow]);
		}
	}
	
	public void populateMaintenanceFields() {
		initializeFields();
		
		if (table.getSelectedRow() <  0) {
			return;
		}
		
		String entry = "";
		
		eventNameField.setText(model.getValueAt(table.getSelectedRow(), 1));
		minAgeField.setText(model.getValueAt(table.getSelectedRow(), 3));
		maxAgeField.setText(model.getValueAt(table.getSelectedRow(), 4));
		timeField.setText(model.getValueAt(table.getSelectedRow(), 5));
		numHeatsField.setText(model.getValueAt(table.getSelectedRow(), 6));
		
		entry = model.getValueAt(table.getSelectedRow(), 0);
		EventComboBox.setComboBoxText(eventCodeBox, entry);
		entry = model.getValueAt(table.getSelectedRow(), 2);
		EventComboBox.setComboBoxText(genderBox, entry);
	}
}
