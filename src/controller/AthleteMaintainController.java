package controller;

import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

import domain.Athlete;
import domain.Event;
import domain.School;
import domain.Teacher;
import ui.AthleteMaintainPanel;
import ui.AthleteTableModel;
import ui.ErrDialog;
import ui.EventComboBox;
import ui.ScorePanel;

public class AthleteMaintainController {
	
	private AthleteMaintainPanel panel;
	private JTable table;
	private AthleteTableModel model;
	
	private JTextField firstField, lastField, ageField;
	private JComboBox eventCodeBox, genderBox, grpBox, schoolBox, teacherBox;
	private ScorePanel scoreField;
	
	public void updateAthleteParams(final AthleteMaintainPanel panel) 
			throws Exception {
		this.panel = panel;
		initializeFields();
		
		if (table.getSelectedRow() < 0) return;
		
		Float score = ScorePanel.NOT_AVAILABLE;
		Integer age = Integer.parseInt(ageField.getText());
		if (!scoreField.getText().equals(""))
			score = Float.parseFloat(scoreField.getText());
		
		if (genderBox.getSelectedIndex() < 0)
			throw new Exception(ErrDialog.GENDER_NS_ERROR);
		else if (eventCodeBox.getSelectedIndex() < 0)
			throw new Exception(ErrDialog.EVENTCD_NS_ERROR);
		else if (grpBox.getSelectedIndex() < 0)
			throw new Exception(ErrDialog.GRPCD_NS_ERROR);
	
		if (eventCodeBox.getSelectedIndex() == -1) 
			throw new Exception(ErrDialog.EVENTCD_NS_ERROR);
		
		else if (teacherBox.getSelectedIndex() == -1) 
			throw new Exception(ErrDialog.TEACHER_NS_ERROR);
		
		else if (schoolBox.getSelectedIndex() == -1) 
			throw new Exception(ErrDialog.SCHOOL_NS_ERROR);
		
		if (age < Athlete.MIN_AGE || age > Athlete.MAX_AGE)
			throw new Exception(ErrDialog.AGE_RANGE_ERROR);
		
		String first = firstField.getText();
		String last = lastField.getText();
		String gender = genderBox.getSelectedItem().toString();
		String school = schoolBox.getSelectedItem().toString();
		String newCode = eventCodeBox.getSelectedItem().toString().toUpperCase();
		String eventName = Event.lookUpByCode(newCode).getName();
		String grpNum = grpBox.getSelectedItem().toString();
		String grpName = teacherBox.getSelectedItem().toString();
		Athlete newAthlete = new Athlete(first, last, gender, age, 
				school, score, newCode, eventName, grpNum, grpName);
		
		/* Checks if input score is within range for the selected event */
		String unit = Event.lookUpByCode(newCode).getScoreUnit();
		float min   = Event.lookUpByCode(newCode).getScoreMin();
		float max   = Event.lookUpByCode(newCode).getScoreMax();
		if (unit != null && 
				unit.equals(ScorePanel.D) || unit.equals(ScorePanel.T)) {
			if (score < min || score > max)
				throw new Exception(ErrDialog.SCORE_RANGE_ERROR + 
					" Range for selected event is " + min + " - " + max + ".");
		}
		
		/* Checks whether athlete will be registered for same event or 
		 * whether he will be registered for too many events */
		int success = model.checkForRedundancies(table, newAthlete);
		if (success == -1) throw new Exception(ErrDialog.ATH_REG_ERROR1);
		if (success == -2) throw new Exception(ErrDialog.ATH_REG_ERROR2);
	
		/* If the event code selected on the table != event code selected from
		 * drop-down, then move the athlete to the new event they're in */
		final Athlete oldAthlete = model.getAthlete(table.getSelectedRow());
		final String oldCode  = oldAthlete.getEventCode();
		final Event  oldEvent = Event.lookUpByCode(oldCode);
		final Event  newEvent = Event.lookUpByCode(newCode);
		if (oldEvent != null && newEvent != null) {
			/* Adds selected athlete to the new event */
			newEvent.addAthlete(newAthlete);
			/* Removes selected athlete from the event they used to be in */
			oldEvent.removeAthlete(newAthlete);
		}
		
		if (table.getSelectedRow() > -1) {
			final int row = table.getSelectedRow();
			
			/* Save all data to prevent against data loss */
			model.deleteAthlete(row);
			int newRow = model.addAthlete(newAthlete, row);
			model.saveData();
			
			/* If athlete's last name changes, update the currently selected row
			 * post-sort */
			if (!newAthlete.generateUniqueKey().equals
					(model.getAthlete(table.getSelectedRow()).generateUniqueKey())) {
				table.getSelectionModel().setSelectionInterval(newRow, newRow);
				table.scrollRectToVisible(new Rectangle(table.getCellRect(newRow, 0, true)));
			}
			
			model.fireTableRowsUpdated(row, row);
		}
	}
	
	public void deleteSelectedAthletes() throws Exception {
		initializeFields();
		final int[] rows = table.getSelectedRows();
		final int lastRow = rows.length - 1;
		if (lastRow >= 0) {
			for (int i = lastRow; i >= 0; i--) {
				final Athlete curAthlete = model.getAthlete(rows[i]);
				String curCode = model.getAthlete(table.getSelectedRow())
						.getEventCode();
				
				/* Remove every athlete from the event they were registered for */
				final Event tempEvent = Event.lookUpByCode(curCode);
				tempEvent.removeAthlete(curAthlete);
				
				/* Removes athlete from table model */
				model.deleteAthlete(rows[i]);
			}
			
			table.clearSelection();
			model.fireTableRowsDeleted(rows[0], lastRow);
			/* Save all data to prevent against data loss */
			model.saveData();
		}
	}
	
	public void populateMaintenanceFields(final AthleteMaintainPanel panel) {
		this.panel = panel;
		initializeFields();
		
		if (table.getSelectedRow() < 0) return;
		
		/* Set values of JTextField objects */
		firstField.setText(model.getValueAt(table.getSelectedRow(), 0));
		lastField.setText(model.getValueAt(table.getSelectedRow(), 1));		
		ageField.setText(model.getValueAt(table.getSelectedRow(), 3));
		scoreField.setText(model.getValueAt(table.getSelectedRow(), 5));
			
		/* Set values of ComboBox objects */
		String entry = "";
		entry = model.getValueAt(table.getSelectedRow(), 2);
		EventComboBox.setComboBoxText(genderBox, entry);
		entry = model.getValueAt(table.getSelectedRow(), 4);
		EventComboBox.setComboBoxText(schoolBox, entry);
		entry = model.getValueAt(table.getSelectedRow(), 6);
		EventComboBox.setComboBoxText(eventCodeBox, entry);
		entry = model.getValueAt(table.getSelectedRow(), 8);
		EventComboBox.setComboBoxText(grpBox, entry);
		entry = model.getValueAt(table.getSelectedRow(), 9);
		EventComboBox.setComboBoxText(teacherBox, entry);
	}
	
	public void populateTeacherBox(
			final JComboBox schoolBox, 
			final JComboBox teacherBox) {
		if (schoolBox != null 
				&& teacherBox != null 
				&& schoolBox.getItemCount() > 0 
				&& schoolBox.getSelectedIndex() > -1) {
			String selection = schoolBox.getSelectedItem().toString();
			HashMap <String, Vector <Teacher>> schoolList = School.getSchoolList();
			
			Vector <Teacher> teacherList = schoolList.get(selection);
			if (teacherList != null) {
				teacherBox.setModel(new DefaultComboBoxModel(teacherList));
				teacherBox.setSelectedIndex(-1);
			}
		}
	}
	
	private void initializeFields() {
		/* Reference variables must point to the most up-to-date objects or 
		 * else the references will point to junk that existed upon this
		 * controller's instantiation
		 */
		table = panel.getTable();
		model = panel.getModel();
		
		firstField = panel.getFirstNameField();
		lastField = panel.getLastNameField();
		ageField = panel.getAgeField();
		scoreField = panel.getScoreField();

		genderBox = panel.getGenderDropDown();
		eventCodeBox = panel.getCodeBox();
		grpBox = panel.getGrpDropDown();
		teacherBox = panel.getTeacherDropDown();
		schoolBox = panel.getSchoolDropDown();
	}
}