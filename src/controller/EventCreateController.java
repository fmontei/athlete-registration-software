package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import domain.Event;
import ui.AthleteCreatePanel;
import ui.AthleteMaintainPanel;
import ui.ErrDialog;
import ui.EventComboBox;
import ui.EventCreatePanel;
import ui.EventMaintainPanel;
import ui.EventTableModel;
import ui.ScorePanel;

public class EventCreateController {
	
	private JTable table;
	private EventTableModel model;
	
	private JTextField eventCodeField, eventNameField;
	private ScorePanel scoreMaxField, scoreMinField;
	private JComboBox scoreBox, sortSeqBox;
	public void createNewEvent() throws Exception {
		initializeFields();

		float scoreMin = ScorePanel.NOT_AVAILABLE;
		float scoreMax = ScorePanel.NOT_AVAILABLE;
		int sortSeq = 0;
		String newCode, newName, scoreUnit;
		
		final String eventCode = eventCodeField.getText();
		/* Checks whether event field is empty */
		if (eventCode.equals(""))
			throw new Exception(ErrDialog.EVENTCD_NULL_ERROR);
		
		else if (Event.verifyEventCode(eventCode) == false)
			throw new Exception(ErrDialog.EVENTCD_WF_ERROR);
		
		else if (eventNameField.getText().equals("")) {
			throw new Exception(ErrDialog.NAME_DNE_ERROR);
		}
		
		/* Validates score */
		if (!(scoreMinField.getText().equals(""))) {
			try {
				scoreMin = Float.parseFloat(scoreMinField.getText());
			} catch(Exception e) { throw new Exception(ErrDialog.SCORE_WF_ERROR); }
			try {
				scoreMax = Float.parseFloat(scoreMaxField.getText());
			} catch(Exception e) { throw new Exception(ErrDialog.SCORE_WF_ERROR); }
		
		if (scoreMin > scoreMax)
			throw new Exception(ErrDialog.SCORE_RANGE_ERROR);
		}
		
		try {
			sortSeq = Integer.parseInt(sortSeqBox.getSelectedItem().toString());
		} catch(Exception e) { throw new Exception(); }
		
		newCode = eventCodeField.getText().toUpperCase();
		newName = eventNameField.getText().toString();
		scoreUnit = scoreBox.getSelectedItem().toString();
		
		Event newEvent = new Event(newCode, newName, scoreUnit, 
				scoreMin, scoreMax, sortSeq);
		
		/* Adds the event code to the event combo boxes */
		EventComboBox.addByCode(newCode, newEvent);
		
		model.addEvent(newEvent);
		final int row = table.getSelectedRow();
		model.fireTableRowsInserted(row, row);
		
		clearAllTextFields();
		updateSortSeq();
	}
	
	public void updateSortSeq() {
		sortSeqBox.addItem(new Integer((sortSeqBox.getItemCount() + 1) * 10));
	}
	
	private void clearAllTextFields() {
		eventCodeField.setText(""); 
		eventNameField.setText(""); 
		sortSeqBox.setSelectedIndex(-1);
		scoreMaxField.setText(""); 
		scoreMinField.setText(""); 
	}
	
	private void initializeFields() {
		/* Reference variables must point to the most up-to-date objects or 
		 * else the references will point to junk that existed upon this
		 * controller's instantiation
		 */
		table = EventMaintainPanel.getTable();
		model = EventMaintainPanel.getModel();
		
		eventCodeField = EventCreatePanel.getCodeField(); 
		eventNameField = EventCreatePanel.getNameField(); 
		scoreMaxField = EventCreatePanel.getScoreMaxField();
		scoreMinField = EventCreatePanel.getScoreMinField();
		sortSeqBox = EventCreatePanel.getsortSeqBox();
		scoreBox = EventCreatePanel.getScoreBox();
	}
}
