package controller;

import java.awt.CardLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import ui.MainFrame;

public class NavController {

	private CardLayout cardLayout;
	private JPanel cardPanel;
	
	public NavController() {
		cardLayout = MainFrame.getCardLayout();
		cardPanel = MainFrame.getCardPanel();
	}
	
	public ImageIcon resizeIcon(ImageIcon icon) {
		Image img = icon.getImage() ;  
		Image newimg = img.getScaledInstance(20, 20, Image.SCALE_SMOOTH);  
		return new ImageIcon(newimg);
	}
	
	public void showCardPanel(final String panelName) {
		cardLayout.show(cardPanel, panelName);
	}
}
