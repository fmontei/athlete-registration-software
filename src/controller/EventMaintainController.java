package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import domain.Athlete;
import domain.Event;
import domain.Heat;
import ui.ErrDialog;
import ui.EventComboBox;
import ui.EventMaintainPanel;
import ui.EventTableModel;
import ui.HeatMaintainPanel;
import ui.ScorePanel;

public class EventMaintainController {

	private EventTableModel model;
	private JTable table;
	private JTextField eventNameField, sortSeqField;
	private ScorePanel  scoreMinField, scoreMaxField;
	private EventComboBox eventCodeBox;
	private JComboBox scoreBox;

	public void initializeFields(){
		
		table = EventMaintainPanel.getTable();
		model = EventMaintainPanel.getModel();
		eventNameField = EventMaintainPanel.getEventNameField();
		eventCodeBox = EventMaintainPanel.getEventCodeBox();
		scoreBox = EventMaintainPanel.getScoreBox();
		scoreMinField = EventMaintainPanel.getScoreMinField();
		scoreMaxField = EventMaintainPanel.getScoreMaxField();
		sortSeqField = EventMaintainPanel.getSortSeqField();
	}
	
	public void updateHeatParams() throws Exception{
		initializeFields();
		
		if (table.getSelectedRow() < 0) {
			return;
		}
		
		Float scoreMin = ScorePanel.NOT_AVAILABLE;
		Float scoreMax = ScorePanel.NOT_AVAILABLE;
		int sortSeq = 0;
		
		if (eventNameField.getText().equals("")) {
			throw new Exception(ErrDialog.NAME_DNE_ERROR);
		}
		
		/* Validates score */
		if (!(scoreMinField.getText().equals(""))) {
			try{
			 scoreMin = Float.parseFloat(scoreMinField.getText());
			} catch(Exception e){
				throw new Exception(ErrDialog.SCORE_WF_ERROR);
			}
			
			try{	
				scoreMax = Float.parseFloat(scoreMaxField.getText());
			} catch(Exception e){
				throw new Exception(ErrDialog.SCORE_WF_ERROR);
			}
			
			if (scoreMin > scoreMax)
				throw new Exception(ErrDialog.SCORE_RANGE_ERROR);
		}
		
		/* Checks if sort sequence field is empty */
		if (sortSeqField.getText().equals(""))
			throw new Exception(ErrDialog.SORT_DNE_ERROR);
		
		/* Checks if sort sequence field is an integer */
		try {
			sortSeq = Integer.parseInt(sortSeqField.getText());
		} catch(Exception e) { 
			throw new Exception(ErrDialog.SORT_WF_ERROR); 
		}
		
		if (table.getSelectedRow() > -1) {
			model.setValueAt(eventCodeBox.getSelectedItem().toString(), 
					table.getSelectedRow(), 0);
			model.setValueAt(eventNameField.getText(), 
					table.getSelectedRow(), 1);
			model.setValueAt(scoreBox.getSelectedItem().toString(), 
					table.getSelectedRow(), 2);
			model.setValueAt(scoreMin.toString(), 
					table.getSelectedRow(), 3);
			model.setValueAt(scoreMax.toString(), 
					table.getSelectedRow(), 4);	
			model.setValueAt(sortSeqField.getText(), 
					table.getSelectedRow(), 5);
			
			model.fireTableRowsUpdated(table.getSelectedRow(), 
					table.getSelectedRow());			
		}
		
	}
	
	public void deleteSelectedEvents() throws Exception {
		final int[] rows = EventMaintainPanel.getTable().getSelectedRows();
		final int lastRow = rows.length - 1;
		if (rows.length > 0) {
			for (int i = lastRow; i >= 0; i--) {
				model.deleteEvent(rows[i]);
			}
			
			model.fireTableRowsDeleted(rows[0], rows[lastRow]);
			clearAllTextFields();
		}
	}
	

	public void populateMaintenanceFields() {
		initializeFields();
		
		if (table.getSelectedRow() <  0) {
			return;
		}
		
		String entry = "";
		eventNameField.setText(model.getValueAt(table.getSelectedRow(), 1));
		scoreMinField.setText(model.getValueAt(table.getSelectedRow(), 3));
		scoreMaxField.setText(model.getValueAt(table.getSelectedRow(), 4));
		sortSeqField.setText(model.getValueAt(table.getSelectedRow(), 5));
		
		entry = model.getValueAt(table.getSelectedRow(), 0);
		EventComboBox.setComboBoxText(eventCodeBox, entry);
		entry = model.getValueAt(table.getSelectedRow(), 2);
		EventComboBox.setComboBoxText(scoreBox, entry);
	}
	
	public void clearAllTextFields() {
		
	}
}