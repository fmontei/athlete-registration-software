package controller;

import java.io.IOException;
import java.util.ArrayList;

import ui.ErrDialog;


import domain.Athlete;
import domain.Event;

public class HeatSheetGenerator {
	public void handleHeatSheetGeneration(){
		//Heat Sheet Generation: This begins when you press the 'Generate Heat Sheets" button in the GUI
		
		// Set comparators' signs to positive 1 so that scores always come
		// out sorted from lowest to highest
		Athlete.resetComparatorSign();
		
		//This will hold the completion dialog at the end
		ErrDialog dia = null;
		 
		HeatSheetController cont = new HeatSheetController();
		ArrayList<Event> evsWProb = new ArrayList<Event>();
       	try {
       			//Use the controller.  This returns a list of events that have athletes that couldn't be added to heats.
				evsWProb = cont.doButton_Click(Event.getEventsSortedBySortSeq());
			} catch (IOException e) {
				
				//Couldn't write to the heat sheet file; let the user know
				dia = new ErrDialog("Error: Problem writing to HeatSheets.txt. Could not generate Heat Sheets.", "Heat Sheet Error");
				dia.setVisible(true);
			} catch (Exception e){
				
				//Some kind of problem occurred, let the user know
				dia = new ErrDialog("Error: Heat sheets could not be created.", "Heat Sheet Error");
				dia.setVisible(true);
			}
       	if(evsWProb.size() > 0){
       		
       		//Let the user know heat sheets were made but may be missing athletes
       		dia = new ErrDialog("Heat sheets created, but some athletes could not be added to some Event(s)", "Heat Sheet");
       		dia.setVisible(true);
       	}
       	else if(dia == null){
       		//I know that this isn't an error, but the ErrDialog class has the needed functionality--it's faster than making
       		//a new class.  Anyway, this means completely successful generation
       		dia = new ErrDialog("Successfully created heat sheets!", "Heat Sheet Success");
       		dia.setVisible(true);
       	}
	}
}
