package controller;

import java.io.IOException;

import datastore.DataStore;

public class DomainTest {

	public static void main(String[] args) {
		DataStore data = DataStore.getInstance();
		
		try {
			data.dumpData();
			data.saveAllData();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
