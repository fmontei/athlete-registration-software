package controller;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextField;

import domain.Event;
import domain.Heat;
import ui.ErrDialog;
import ui.EventComboBox;
import ui.EventMaintainPanel;
import ui.EventTableModel;
import ui.HeatCreatePanel;
import ui.HeatMaintainPanel;
import ui.HeatTableModel;
import ui.ScorePanel;

public class HeatCreateController {

	private JTable table;
	private HeatTableModel model;
	private JTextField minAgeField, maxAgeField, eventNameField, numHeatsField;
	private ScorePanel timeField;
	private JComboBox eventCodeBox, genderBox;
	
	public void createNewHeat() throws Exception {
		initializeFields();
	
		int minAge, maxAge, numHeats;
		String eventName, eventCode, gender,time;
		
		try {
		minAge = Integer.parseInt(minAgeField.getText());
		} catch(Exception e) { throw new Exception(ErrDialog.AGE_RANGE_ERROR); }
		try {
		maxAge = Integer.parseInt(maxAgeField.getText());
		} catch(Exception e) { throw new Exception(ErrDialog.AGE_RANGE_ERROR); }
		
		eventName = eventNameField.getText().toString();
		try{
		eventCode = eventCodeBox.getSelectedItem().toString();
		} catch(Exception e){
			e.printStackTrace();
			throw new Exception(ErrDialog.EVENTCD_NS_ERROR);
		}
		try{
		gender = genderBox.getSelectedItem().toString();
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception(ErrDialog.GRPCD_NS_ERROR);
		}
		time = timeField.getText().toString();
		numHeats = Integer.parseInt(numHeatsField.getText().toString());
		
		final Event newEvent = Event.lookUpByCode(eventCode);
		Heat newHeat = new Heat(eventCode, eventName, gender, 
				minAge, maxAge, time, numHeats);
		
		/* Updates the athlete list for the event the athlete is registered for */
		Event temp = Event.lookUpByCode(eventCode);
		temp.addHeat(newHeat);
		
		/* Adds new athlete to bottom of table; repaints last table row */
		model.addHeat(newHeat);
		final int row = table.getSelectedRow();
		model.fireTableRowsInserted(row, row);
		
		clearAllTextFields();
	
	}
	
	private void clearAllTextFields() {
		minAgeField.setText(""); 
		maxAgeField.setText(""); 
		timeField.setText(""); 
		eventNameField.setText("");
		genderBox.setSelectedIndex(0);
		eventCodeBox.setSelectedIndex(-1);
		numHeatsField.setText("");
	}
	
	private void initializeFields() {
		/* Reference variables must point to the most up-to-date objects or 
		 * else the references will point to junk that existed upon this
		 * controller's instantiation
		 */
		table = HeatMaintainPanel.getTable();
		model = HeatMaintainPanel.getModel();
		
		minAgeField = HeatCreatePanel.getMinAgeField();
		maxAgeField = HeatCreatePanel.getMaxAgeField();
		timeField = HeatCreatePanel.getTimeField();
		eventNameField = HeatCreatePanel.getEventNameField();
		eventCodeBox = HeatCreatePanel.getEventCodeBox();
		genderBox = HeatCreatePanel.getGenderBox();
		numHeatsField = HeatCreatePanel.getNumHeatsField();
	}
}
