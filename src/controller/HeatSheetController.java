package controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import domain.Event;
import domain.HeatSheetDoc;

public class HeatSheetController {
	private String fileToSaveTo;
	private HeatSheetDoc doc;
	public HeatSheetController(){
		fileToSaveTo = "HeatSheets.txt";
		doc = new HeatSheetDoc();
	}

	public ArrayList<Event> doButton_Click(List<Event> list) throws IOException, Exception{
		
		//Get ready to return a list of problem events
		ArrayList<Event> eventsWithIssues = new ArrayList<Event>();
		
		int err = 0;
		
		//loop through events
		for(int i=0; i<list.size(); i++){
			err = 0;
			try{
				//generate heat sheets for each event
				err = list.get(i).generateSheets(doc);
			}catch(Exception e){
				
			}
			if(i!= list.size()-1 && !list.get(i).getHeats().isEmpty()){
				//This will fire if heat sheets were successfully generated and this is not the final 
				//event--it just makes a page break
				doc.append((char)12);
			}
			
			if(err > 0){
				//If there was an issue and athletes couldn't be added to heats, this happens
				eventsWithIssues.add(list.get(i));
			}
			
			
		}
		String dbl = ""+(char)12+(char)12;
		
		String sngl = ""+(char)12;
		String docString = doc.getHeatSheets();
		
		while(doc.getHeatSheets() != (docString = docString.replace(dbl,sngl))){
			doc.setHeatSheets(docString);
		}
		
		//Save
		saveToFile();
		
		return eventsWithIssues;
	}
	public void saveToFile() throws IOException, FileNotFoundException {
		//Save to the file
		File file = new File(fileToSaveTo);
		if(!file.exists()){
			file.createNewFile();
		}
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
		out.write(doc.getHeatSheets().getBytes());
		out.close();
		
	}
}
