To run the following application,

1) Download final.zip from https://bitbucket.org/fmontei/athlete-registration-software/downloads
2) Unpack everything into a folder
3) Execute final.jar by double-clicking on it

Alternatively, after downloading final.zip, type the following into the command prompt: 
	unzip final.zip -d /temp/
	cd temp
	java -jar final.jar